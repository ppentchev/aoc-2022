{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [ pkgs.cargo ];
  shellHook = ''
    set -e
    export NIX_ENFORCE_PURITY=0
    cargo clean
    cargo build
    sh data/run-tests.sh target/debug/aoc-2022 data
    exit
  '';
}
