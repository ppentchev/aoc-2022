{ pkgs ? import <nixpkgs> {}, ruby-ver ? "3_1" }:
let
  ruby-name = "ruby_${ruby-ver}";
  ruby = builtins.getAttr ruby-name pkgs;
  ruby-pkgs = ruby.withPackages (p: with p; [ rubocop ]);
in pkgs.mkShell {
  buildInputs = [ ruby-pkgs ];
  shellHook = ''
    set -e
    rubocop ruby
    exit
  '';
}
