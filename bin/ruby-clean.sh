#!/bin/sh
#
# Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e
set -x

ruby_dir()
{
	local binpath bindir topdir

	binpath="$(readlink -f -- "$0")"
	bindir="$(dirname -- "$binpath")"
	topdir="$(dirname -- "$bindir")"
	printf -- '%s/ruby' "$topdir"
}

ruby_envdir()
{
	local rubydir="$1"
	local envdir=''
	
	cd -- "$rubydir"
	envdir="$(ruby -e 'require "yaml"; puts File.dirname(YAML.load_file("'"$rubydir"'/.bundle/config")["BUNDLE_PATH"])')"
	printf -- '%s' "$(readlink -f -- "$envdir")"
	cd -- "$cwd"
}

cwd="$(pwd)"
rubydir="$(ruby_dir)"
envdir="$(ruby_envdir "$rubydir")"

rm -rf -- "$envdir" "$rubydir/../doc-ruby" "$rubydir/../.yardoc"
