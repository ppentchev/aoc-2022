#!/bin/sh
#
# Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

usage()
{
	cat <<'EOUSAGE'
Usage:	run-tests /path/to/program /path/to/datadir
EOUSAGE
}

run_test()
{
	local program="$1" datadir="$2" test="$3"

	echo "Running the $test test for $program"

	echo '- trivial'
	"$program" "$test" -- "$datadir/${test%-*}-trivial-input.txt" > "$tempf"
	cat -- "$tempf"
	diff -u -- "$datadir/$test-trivial-output.txt" "$tempf"

	echo '- real'
	"$program" "$test" -- "$datadir/${test%-*}-real-input.txt" > "$tempf"
	cat -- "$tempf"
	diff -u -- "$datadir/$test-real-output.txt" "$tempf"
}

run_tests()
{
	local program="$1" datadir="$2"
	local line

	for line in $($program implemented); do
		case "$line" in
			[0-9][0-9]-[12])
				run_test "$program" "$datadir" "$line"
				;;

			*)
				echo "Unexpected output from '$program implemented': $line" 1>&2
				exit 1
				;;
		esac
	done
}

if [ "$#" -ne 2 ]; then
	usage 1>&2
	exit 1
fi

tempf="$(mktemp run-aoc-tests.txt.XXXXXX)"
# shellcheck disable=SC2064  # Doing this on purpose.
trap "rm -f -- '$tempf'" EXIT HUP INT TERM QUIT

run_tests "$1" "$2"
