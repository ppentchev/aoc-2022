# typed: strict
# frozen_string_literal: true

# Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

require 'set'
require 'sorbet-runtime'

# Fixing the signal transceiver / locator
module AoC2022::Signal
  extend T::Sig

  # The common library configuration class.
  Config = AoC2022::Defs::Config

  module_function

  sig { params(cfg: Config).returns(T::Array[String]) }
  # Read lines of text from the input file.
  def parse_from_file(cfg)
    File.readlines(cfg.filename, chomp: true)
  end

  sig { params(line: String, count: Integer).returns(String) }
  # Find the first appearance of `count` different characters in the specified line.
  def handle_line(line, count)
    res = line.chars.each_cons(count).with_index.find { |sigs, _| Set.new(sigs).length == count }
    Kernel.abort "Nothing found in #{line}" if res.nil?
    (res[1] + count).to_s
  end

  sig { params(cfg: Config).returns(String) }
  # Find the first appearance of four different characters.
  def test061(cfg)
    parse_from_file(cfg).map { |line| handle_line(line, 4) }.join(' ')
  end

  sig { params(cfg: Config).returns(String) }
  # Find the first appearance of fourteen different characters.
  def test062(cfg)
    parse_from_file(cfg).map { |line| handle_line(line, 14) }.join(' ')
  end
end
