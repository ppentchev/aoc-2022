# typed: strict
# frozen_string_literal: true

# Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

require 'sorbet-runtime'

# Things related to elves carrying food around
module AoC2022::Food
  extend T::Sig

  # The common library configuration class.
  Config = AoC2022::Defs::Config

  # Keep things together while reading file lines
  class Accumulator
    extend T::Sig

    sig { returns(T::Array[Integer]) }
    # The list of weights stashed so far
    attr_reader :acc

    sig { returns(Integer) }
    # The currently-summed-up weight
    attr_reader :current

    sig { params(acc: T::Array[Integer], current: Integer).void }
    def initialize(acc = [], current = 0)
      @acc = T.let(acc, T::Array[Integer])
      @current = T.let(current, Integer)
      freeze
    end

    sig { returns(T.self_type) }
    # Stash the weight accumulated so far into the list
    def stash
      self.class.new(@acc + [@current], 0)
    end

    sig { returns(T.self_type) }
    # If we have any weight accumulated, stash one last time
    def stash_final
      if @current.zero?
        self
      else
        stash
      end
    end

    sig { params(weight: Integer).returns(T.self_type) }
    # Pile more weight onto the current elf
    def pile_on(weight)
      self.class.new(@acc, @current + weight)
    end
  end

  module_function

  sig { params(lines: T::Array[String]).returns(T::Array[Integer]) }
  # Parse successive lines into elven burden weights.
  def parse_lines(lines)
    lines.reduce(Accumulator.new) do |acc, line|
      line == '' ? acc.stash : acc.pile_on(line.to_i)
    end.stash_final.acc
  end

  sig { params(filename: String).returns(T::Array[Integer]) }
  # Read lines from a file, parse them into elven burden weights.
  def parse_from_file(filename)
    parse_lines File.read(filename).lines(chomp: true)
  end

  sig { params(cfg: Config, count: Integer).returns(T::Array[Integer]) }
  # Read the weights carried by elves from a file
  def read_elven_weights(cfg, count)
    acc = parse_from_file cfg.filename

    res = acc.sort.reverse[..(count - 1)]
    Kernel.abort 'Not enough elves' if res.nil?
    res
  end

  sig { params(cfg: Config).returns(String) }
  # Figure out how many calories the stupidest elf is burdened with.
  def test011(cfg)
    weights = read_elven_weights cfg, 1
    weights.sum.to_s
  end

  sig { params(cfg: Config).returns(String) }
  # Wait, there is more than one stupid elf?
  def test012(cfg)
    weights = read_elven_weights cfg, 3
    weights.sum.to_s
  end
end
