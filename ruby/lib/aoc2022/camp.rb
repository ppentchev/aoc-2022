# typed: strict
# frozen_string_literal: true

# Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

require 'sorbet-runtime'

# All kinds of stuff to do around the elven camp.
module AoC2022::Camp
  extend T::Sig

  # The common library configuration class.
  Config = AoC2022::Defs::Config

  # A single elf's assignment of sections to clean up.
  Sections = T.type_alias { T::Range[Integer] }

  # The sections assigned to a pair of elves.
  Pair = T.type_alias { [Sections, Sections] }

  module_function

  sig { params(line: String).returns(Pair) }
  # Parse a single line into two pairs (ranges) of integers.
  def parse_line(line)
    arr = line.split(',').map { |pair| pair.split('-').map(&:to_i) }.sort
    first = arr.fetch(0)
    second = arr.fetch(1)
    [first.fetch(0)..first.fetch(1), second.fetch(0)..second.fetch(1)]
  end

  sig { params(lines: T::Array[String]).returns(T::Array[Pair]) }
  # Parse text lines into elven pairs and stuff.
  def parse_lines(lines)
    lines.map { |line| parse_line(line) }
  end

  sig { params(filename: String).returns(T::Array[Pair]) }
  # Parse the input data from the specified file
  def parse_from_file(filename)
    parse_lines File.read(filename).lines(chomp: true)
  end

  sig { params(line: Pair).returns(T::Boolean) }
  # Do these two ranges overlap fully?
  def overlap_fully?(line)
    line[0].cover?(line[1]) || line[1].cover?(line[0])
  end

  sig { params(cfg: Config).returns(String) }
  # See whose elves' assignments overlap completely.
  def test041(cfg)
    parse_from_file(cfg.filename).count { |line| overlap_fully?(line) }.to_s
  end

  sig { params(cfg: Config).returns(String) }
  # See whose elves' assignments overlap completely.
  def test042(cfg)
    parse_from_file(cfg.filename).count { |line| line[0].last >= line[1].first }.to_s
  end
end
