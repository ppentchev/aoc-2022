# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'aoc2022'
  s.version = '0.1.0'
  s.summary = 'Advent of Code 2022 solvers'
  s.description = File.read(File.join(File.dirname(__FILE__), 'README.md'))
  s.authors = ['Peter Pentchev']
  s.email = 'roam@ringlet.net'
  s.files = [
    'lib/aoc2022.rb',
    'lib/aoc2022/defs.rb',
    'lib/aoc2022/food.rb'
  ]
  s.executables = [
    'aoc-2022'
  ]
  s.homepage = 'https://gitlab.com/ppentchev/aoc-2022'
  s.license = 'BSD-2-Clause'
  s.metadata = {
    'rubygems_mfa_required' => 'false'
  }
  s.required_ruby_version = '>= 3.0'
end
