# Peter Pentchev's Advent of Code 2022 solvers

If you are reading this, then you already know what this repo is about.
But just in case you don't, take a look at the [Advent of Code][aoc] website.

## Testing the Rust implementation

Build the Rust program, then run `data/run-tests.sh` with the path to
the compiled program and the path to the data/ directory,
e.g. something like:

    cargo build
    data/run-tests.sh target/debug/aoc-2022 data

## Testing the Ruby implementation

Some of these steps are very un-Ruby-like. They will be corrected soon, I think.

Create the Ruby development environment, then run `data/run-tests.sh` with
the path to the helper script that runs the Ruby program within that environment:

    bin/ruby-prep.sh
    data/run-tests.sh bin/ruby-run.sh data

[aoc]: https://adventofcode.com/
