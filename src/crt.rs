/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Figure out what the device's CRT display shows.

use std::collections::VecDeque;
use std::fs;

use anyhow::{anyhow, Context};
use nom::{
    branch::alt, bytes::complete::tag, character::complete::i32 as p_i32,
    combinator::all_consuming, sequence::preceded, IResult,
};
use tracing::trace;

use crate::defs::{Config, Error};

/// A single instruction.
#[derive(Debug, Copy, Clone)]
enum Ins {
    /// An 'add something to something else' instruction.
    AddX(i32),

    /// A 'no operation' instruction.
    Noop,
}

/// Parse an 'add' instruction.
fn p_ins_addx(input: &str) -> IResult<&str, Ins> {
    let (r_input, value) = preceded(tag("addx "), p_i32)(input)?;
    Ok((r_input, Ins::AddX(value)))
}

/// Parse a 'noop' instruction.
fn p_ins_noop(input: &str) -> IResult<&str, Ins> {
    let (r_input, _) = tag("noop")(input)?;
    Ok((r_input, Ins::Noop))
}

/// Parse a single instruction.
fn p_line(input: &str) -> IResult<&str, Ins> {
    all_consuming(alt((p_ins_addx, p_ins_noop)))(input)
}

/// The processor state.
#[derive(Debug)]
enum State {
    /// An error was encountered already.
    Error,
    /// At the start of an instruction.
    Start,

    /// In the middle of an 'addx' instruction.
    Halfway(i32),
}

/// The CRT control processor.
#[derive(Debug)]
struct Processor {
    /// The program to execute.
    prog: VecDeque<Ins>,

    /// The current processor state.
    state: State,

    /// The current X register value.
    x: i32,
}

impl Processor {
    /// Initialize the processor.
    pub fn new(prog: &[Ins]) -> Self {
        Self {
            prog: prog.iter().copied().collect(),
            state: State::Start,
            x: 1,
        }
    }
}

impl Iterator for Processor {
    type Item = Result<i32, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let current = self.x;

        let (new_state, res) = match self.state {
            State::Start => match self.prog.pop_front() {
                None => (State::Start, None),
                Some(Ins::Noop) => (State::Start, Some(Ok(current))),
                Some(Ins::AddX(value)) => (State::Halfway(value), Some(Ok(current))),
            },
            State::Halfway(value) => match current
                .checked_add(value)
                .with_context(|| format!("Could not add {} to {}", value, current))
                .map_err(Error::Value)
            {
                Ok(new_x) => {
                    self.x = new_x;
                    (State::Start, Some(Ok(current)))
                }
                Err(err) => (State::Error, Some(Err(err))),
            },
            State::Error => (
                State::Error,
                Some(Err(Error::Value(anyhow!(
                    "The processor already reported an error"
                )))),
            ),
        };
        self.state = new_state;
        res
    }
}

/// Read the instructions to execute from the input file.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parse errors.
fn read_prog<C: Config>(cfg: &C) -> Result<Vec<Ins>, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    contents
        .lines()
        .map(|line| {
            Ok(p_line(line)
                .map_err(|err| err.map_input(ToOwned::to_owned))
                .with_context(|| {
                    format!("Could not parse the '{}' input line", line.escape_debug())
                })
                .map_err(Error::Value)?
                .1)
        })
        .collect::<Result<_, _>>()
}

/// Sample the signal strength during six cycles.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on inconsistent values or integer overflow.
#[inline]
pub fn test_10_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let prog = read_prog(cfg)?;
    let total = [20, 40, 40, 40, 40, 40]
        .into_iter()
        .try_fold(
            (Processor::new(&prog), 0_i32, 0),
            |(mut cpu, acc, time_acc): (Processor, i32, usize), time_ofs| {
                let total_time = time_acc
                    .checked_add(time_ofs)
                    .with_context(|| {
                        format!("We should be able to add {} to {}", time_ofs, time_acc)
                    })
                    .map_err(Error::Internal)?;
                let values = (time_acc..total_time)
                    .into_iter()
                    .map(|idx| {
                        cpu.next()
                            .with_context(|| {
                                format!("Could not get element {} from {:?}", idx, cpu)
                            })
                            .map_err(Error::Value)?
                            .with_context(|| {
                                format!("Something wrong with element {} from {:?}", idx, cpu)
                            })
                            .map_err(Error::Value)
                    })
                    .collect::<Result<Vec<_>, _>>()?;
                let value = values
                    .last()
                    .with_context(|| format!("What, no {} element in {:?}", total_time, cpu))
                    .map_err(Error::Value)?;
                let i_time = i32::try_from(total_time)
                    .with_context(|| format!("Could not convert {} to i32", total_time))
                    .map_err(Error::Internal)?;
                let product = i_time
                    .checked_mul(*value)
                    .with_context(|| format!("Could not multiply {} by {}", i_time, *value))
                    .map_err(Error::Value)?;
                trace!(product);
                let acc_updated = acc
                    .checked_add(product)
                    .with_context(|| format!("Could not add {} to {}", product, acc))
                    .map_err(Error::Value)?;
                trace!(acc_updated);
                Ok((cpu, acc_updated, total_time))
            },
        )?
        .1;
    Ok(total.to_string())
}

/// Race the beam.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on inconsistent values or integer overflow.
#[inline]
pub fn test_10_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let prog = read_prog(cfg)?;

    let rows = (0_i32..6_i32)
        .try_fold(
            (Processor::new(&prog), String::new()),
            |(row_start_cpu, rows_acc), row_idx| {
                (0_i32..40_i32).try_fold(
                    (row_start_cpu, rows_acc),
                    |(mut col_cpu, mut col_acc), col_idx: i32| {
                        let sprite = col_cpu
                            .next()
                            .with_context(|| {
                                format!(
                                    "Can't get sprite at ({}, {}): {:?}",
                                    col_idx, row_idx, col_cpu
                                )
                            })
                            .map_err(Error::Value)?
                            .with_context(|| {
                                format!(
                                    "Error for sprite at ({}, {}): {:?}",
                                    col_idx, row_idx, col_cpu
                                )
                            })
                            .map_err(Error::Value)?;
                        if col_idx.abs_diff(sprite) < 2 {
                            col_acc.push('#');
                        } else {
                            col_acc.push('.');
                        }
                        Ok((col_cpu, col_acc))
                    },
                )
            },
        )?
        .1;
    trace!("{}", rows.len());
    let lines = (0_i32..6_i32)
        .fold((rows, Vec::new()), |(mut start, mut acc), _| {
            let rest = start.split_off(40);
            acc.push(start);
            (rest, acc)
        })
        .1;
    Ok(lines.join("\n"))
}
