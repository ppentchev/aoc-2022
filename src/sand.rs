/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! A myriad grains of sand, all falling down in a cave.

use std::cmp::{self, Ordering};
use std::collections::HashMap;
use std::fs;
use std::iter;

use anyhow::{anyhow, Context};
use itertools::{FoldWhile, Itertools};
use nom::{
    bytes::complete::tag,
    character::complete::u32 as p_u32,
    combinator::all_consuming,
    error::{Error as NError, ErrorKind as NErrorKind},
    multi::{many1, separated_list1},
    sequence::{separated_pair, terminated},
    Err as NErr, IResult,
};
use tracing::trace;

use crate::defs::{Config, Direction, Error, MapXY, PosXY};

/// The position of a square within the vertical slice of the cave.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Pos {
    /// The X coordinate.
    x: u32,

    /// The Y coordinate.
    y: u32,
}

impl PosXY for Pos {
    type Coord = u32;

    fn x(&self) -> Self::Coord {
        self.x
    }

    fn y(&self) -> Self::Coord {
        self.y
    }

    fn from_xy(x: Self::Coord, y: Self::Coord) -> Self {
        Self { x, y }
    }
}

/// The vertical slice of the cave.
#[derive(Debug, PartialEq, Eq)]
struct Map {
    /// The upper left corner of the rectangle containing all the rocks.
    pos_min: Pos,

    /// The lower right corner of the rectangle containing all the rocks.
    pos_max: Pos,

    /// Has this place been taken up? And was it by a rock?
    heights: HashMap<Pos, bool>,

    /// Does this map have a floor?
    has_floor: bool,
}

/// What happens on a single step for a grain of sand.
#[derive(Debug, PartialEq, Eq)]
enum Dropped {
    /// It rests on a piece of rock or on another grain of sand.
    Rest,

    /// It escapes and falls down, down, down...
    Escape,

    /// It moves down and possibly a bit to the side.
    Move(Pos),
}

impl MapXY<Pos, bool> for Map {
    fn size(&self) -> Pos {
        self.pos_max
    }

    fn start(&self) -> Pos {
        Pos { x: 500, y: 0 }
    }

    // This one holds no meaning for this particular case.
    // OVERRIDE: No, but really.
    #[allow(clippy::unreachable)]
    fn end(&self) -> Pos {
        unreachable!()
    }

    fn heights(&self) -> &HashMap<Pos, bool> {
        &self.heights
    }
}

impl Map {
    /// Add a floor to the map, adjust `pos_min` and `pos_max`.
    fn with_floor(self) -> Result<Self, Error> {
        if self.has_floor {
            return Err(Error::Internal(anyhow!("This map already has a floor")));
        }

        let floor_y = self
            .pos_max
            .y
            .checked_add(1)
            .with_context(|| {
                format!(
                    "We should have been able to add 1 to pos_max.y {}",
                    self.pos_max.y
                )
            })
            .map_err(Error::Process)?;
        let pos_min = Pos {
            x: cmp::min(500_u32.saturating_sub(floor_y), self.pos_min.x),
            y: self.pos_min.y,
        };
        let pos_max = Pos {
            x: cmp::max(500_u32.saturating_add(floor_y), self.pos_max.x),
            y: floor_y,
        };
        Ok(Self {
            pos_min,
            pos_max,
            heights: self.heights,
            has_floor: true,
        })
    }

    /// Drop a single grain of sand one step further.
    // OVERRIDE: We cannot fix tracing::instrument.
    #[allow(clippy::panic_in_result_fn)]
    #[allow(clippy::unreachable)]
    #[tracing::instrument(skip(self))]
    fn drop_sand_single(&self, pos: &Pos) -> Result<Dropped, Error> {
        let next_y = pos
            .y
            .checked_add(1)
            .with_context(|| format!("We should have been able to add 1 to {}", pos.y))
            .map_err(Error::Process)?;
        {
            let npos = Pos {
                x: pos.x,
                y: next_y,
            };
            if !self.heights.contains_key(&npos) {
                return Ok(Dropped::Move(npos));
            }
        }
        trace!(next_y);
        match pos.x.cmp(&self.pos_min.x) {
            Ordering::Greater => {
                let npos = Pos {
                    x: pos
                        .x
                        .checked_sub(1)
                        .with_context(|| {
                            format!("We should have been able to subtract one from {}", pos.x)
                        })
                        .map_err(Error::Process)?,
                    y: next_y,
                };
                trace!(?npos);
                if !self.heights.contains_key(&npos) {
                    return Ok(Dropped::Move(npos));
                }
            }
            Ordering::Equal => {
                // It dropped off the left side.
                return Ok(Dropped::Escape);
            }
            Ordering::Less => {
                return Err(Error::Internal(anyhow!(format!(
                    "How did we get to ({}, {})?",
                    pos.x, pos.y
                ))));
            }
        };
        match pos.x.cmp(&self.pos_max.x) {
            Ordering::Less => {
                let npos = Pos {
                    x: pos
                        .x
                        .checked_add(1)
                        .with_context(|| {
                            format!("We should have been able to add one to {}", pos.x)
                        })
                        .map_err(Error::Process)?,
                    y: next_y,
                };
                trace!(?npos);
                if !self.heights.contains_key(&npos) {
                    return Ok(Dropped::Move(npos));
                }
            }
            Ordering::Equal => {
                // It dropped off the left side.
                return Ok(Dropped::Escape);
            }
            Ordering::Greater => {
                return Err(Error::Internal(anyhow!(format!(
                    "How did we get to ({}, {})?",
                    pos.x, pos.y
                ))));
            }
        };

        // It seems it's blocked, is it not?
        Ok(Dropped::Rest)
    }

    /// Drop a single grain of sand, see where it ends up.
    // OVERRIDE: We cannot fix tracing::instrument.
    #[allow(clippy::panic_in_result_fn)]
    #[allow(clippy::unreachable)]
    #[tracing::instrument(skip(self))]
    fn drop_sand(&mut self) -> Result<bool, Error> {
        trace!(?self.heights);
        let mut pos = self.start();
        if self.heights.contains_key(&pos) {
            if self.has_floor {
                return Ok(false);
            }
            return Err(Error::Process(anyhow!(format!(
                "How did we get to ({}, {}) not being free?",
                pos.x, pos.y
            ))));
        }

        loop {
            trace!(?pos);
            if pos.y == self.pos_max.y {
                if self.has_floor {
                    self.heights.insert(pos, false);
                    return Ok(true);
                }
                return Ok(false);
            }
            if pos.y > self.pos_max.y {
                return Err(Error::Internal(anyhow!(format!(
                    "We should not have ended up at {:?} with {:?}",
                    pos, self,
                ))));
            }
            match {
                let drop = self
                    .drop_sand_single(&pos)
                    .with_context(|| format!("Calculating a move from ({}, {})", pos.x, pos.y))
                    .map_err(Error::Process)?;
                trace!(?drop);
                drop
            } {
                Dropped::Escape => {
                    if self.has_floor {
                        return Err(Error::Process(anyhow!(format!(
                            "How did we get to ({}, {}) and escape?",
                            pos.x, pos.y
                        ))));
                    }
                    return Ok(false);
                }
                Dropped::Rest => {
                    self.heights.insert(pos, false);
                    return Ok(true);
                }
                Dropped::Move(npos) => {
                    if self.heights.contains_key(&npos) {
                        return Err(Error::Internal(anyhow!(format!(
                            "Tried to move from ({}, {}) to already taken ({}, {})",
                            pos.x, pos.y, npos.x, npos.y
                        ))));
                    }
                    pos = npos;
                }
            }
        }
    }
}

/// A single line in the rock formation definitions.
#[derive(Debug, PartialEq, Eq)]
struct Line {
    /// The starting position.
    start: Pos,

    /// The direction of the line of rocks.
    dir: Direction,

    /// The number of steps to take in said direction.
    count: usize,
}

/// Parse a single position within a rock formation line.
fn p_pos(input: &str) -> IResult<&str, Pos> {
    let (r_input, (x, y)) = separated_pair(p_u32, tag(","), p_u32)(input)?;
    Ok((r_input, Pos { x, y }))
}

/// Parse a single rock formation line.
fn p_rock(input: &str) -> IResult<&str, Vec<Line>> {
    let (r_input, points) = terminated(separated_list1(tag(" -> "), p_pos), tag("\n"))(input)?;
    let nerr_fail = || NErr::Failure(NError::new(r_input, NErrorKind::Fail));
    // let ok_or_nerr = |opt| opt.ok_or_else(nerr_fail);

    let mut points_iter = points.into_iter();
    let first = points_iter.next().ok_or_else(nerr_fail)?;
    let res = points_iter.try_fold((Vec::new(), first), |(mut acc, start), pos| {
        // OVERRIDE: Yes, for the present we cannot pass the original error through.
        #[allow(clippy::map_err_ignore)]
        let (i_start_x, i_start_y, i_pos_x, i_pos_y) = {
            let start_x = i32::try_from(start.x).map_err(|_| nerr_fail())?;
            let start_y = i32::try_from(start.y).map_err(|_| nerr_fail())?;
            let pos_x = i32::try_from(pos.x).map_err(|_| nerr_fail())?;
            let pos_y = i32::try_from(pos.y).map_err(|_| nerr_fail())?;
            (start_x, start_y, pos_x, pos_y)
        };
        let dx = i_pos_x.checked_sub(i_start_x).ok_or_else(nerr_fail)?;
        let dy = i_pos_y.checked_sub(i_start_y).ok_or_else(nerr_fail)?;
        if (dx == 0_i32 && dy == 0_i32) || (dx != 0_i32 && dy != 0_i32 && dx != dy) {
            Err(nerr_fail())
        } else {
            let dir = Direction::from_xy(dx, dy);
            // OVERRIDE: Yes, for the present we cannot pass the original error through.
            #[allow(clippy::map_err_ignore)]
            let count = usize::try_from(cmp::max(dx.abs(), dy.abs())).map_err(|_| nerr_fail())?;
            acc.push(Line { start, dir, count });
            Ok((acc, pos))
        }
    })?;
    Ok((r_input, res.0))
}

/// Parse the description of all rock formations into a series of lines.
fn p_all_rocks(input: &str) -> IResult<&str, Vec<Line>> {
    let (r_input, rock_lines) = all_consuming(many1(p_rock))(input)?;
    let res = rock_lines.into_iter().concat();
    Ok((r_input, res))
}

/// Find the bounding rectangle, the one that contains all the rocks.
fn get_min_max(heights: &HashMap<Pos, bool>) -> Result<(Pos, Pos), Error> {
    let (pos_min, pos_max) = heights
        .keys()
        .copied()
        .zip(heights.keys().copied())
        .reduce(|(c_min, c_max), (item_min, item_max)| {
            (
                Pos {
                    x: cmp::min(c_min.x, item_min.x),
                    y: cmp::min(c_min.y, item_min.y),
                },
                Pos {
                    x: cmp::max(c_max.x, item_max.x),
                    y: cmp::max(c_max.y, item_max.y),
                },
            )
        })
        .context("Not a single rock?")
        .map_err(Error::Value)?;
    if heights.contains_key(&Pos { x: 500, y: 0 }) {
        return Err(Error::Value(anyhow!(
            "The starting point (500, 0) is not free"
        )));
    }
    trace!(?pos_min);
    trace!(?pos_max);

    if pos_min.x > 500 || pos_max.x < 500 {
        return Err(Error::Value(anyhow!(
            "The rocks are all on one side of the 500 line"
        )));
    }
    Ok((pos_min, pos_max))
}

/// Parse the description of rock formations into a map.
fn parse_rocks(input: &str) -> Result<Map, Error> {
    let (_, lines) = p_all_rocks(input)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the rocks data")
        .map_err(Error::Value)?;
    /* Iterate over all the squares along all the lines. */
    let heights: HashMap<Pos, bool> = lines
        .into_iter()
        .map(|line| {
            /*
             * Start from the `start` point.
             * Iterate `count` times: move one square, output the current position.
             */
            Ok(iter::once(line.start).chain(
                (0..line.count)
                    .try_fold((Vec::new(), line.start), move |(mut acc, last), _| {
                        let current = last.step(line.dir)?;
                        acc.push(current);
                        Ok((acc, current))
                    })?
                    .0
                    .into_iter(),
            ))
        })
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .flatten()
        .map(|pos| (pos, true))
        .collect();
    let (pos_min, pos_max) = get_min_max(&heights)?;
    Ok(Map {
        pos_min,
        pos_max,
        heights,
        has_floor: false,
    })
}

/// Read the lines, build the hashmap of rocks and hard places.
fn read_map<C: Config>(cfg: &C, has_floor: bool) -> Result<Map, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let res = parse_rocks(&contents)?;
    if has_floor {
        res.with_floor()
            .context("Could not add a floor to the map")
            .map_err(Error::Process)
    } else {
        Ok(res)
    }
}

/// Run the whole thing, with or without a floor.
fn run<C: Config>(cfg: &C, has_floor: bool) -> Result<String, Error> {
    let start_map = read_map(cfg, has_floor)?;
    match (0_usize..).fold_while(Ok((start_map, 0)), |acc_res, current_count| match acc_res {
        Ok((mut map, _)) => match map.drop_sand() {
            Ok(true) => FoldWhile::Continue(Ok((map, current_count))),
            Ok(false) => FoldWhile::Done(Ok((map, current_count))),
            Err(err) => FoldWhile::Done(Err(err)),
        },
        Err(err) => FoldWhile::Done(Err(err)),
    }) {
        FoldWhile::Done(Ok((_, count))) => Ok(count.to_string()),
        FoldWhile::Continue(Ok((map, count))) => Err(Error::Internal(anyhow!(format!(
            "Something weird happened at turn {}: {:?}",
            count, map
        )))),
        FoldWhile::Done(Err(err)) | FoldWhile::Continue(Err(err)) => Err(err),
    }
}

/// How many grains of sand...
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parse errors or inconsistent data.
#[inline]
pub fn test_14_1<C: Config>(cfg: &C) -> Result<String, Error> {
    run(cfg, false)
}

/// How many grains of sand, no abyss.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parse errors or inconsistent data.
#[inline]
pub fn test_14_2<C: Config>(cfg: &C) -> Result<String, Error> {
    run(cfg, true)
}

#[cfg(test)]
// OVERRIDE: This is a test suite.
#[allow(clippy::panic_in_result_fn)]
mod tests {
    use anyhow::{Context, Result};
    use once_cell::sync::Lazy;
    use rstest::rstest;
    use tracing_test::traced_test;

    use crate::defs::Direction;

    use super::{Dropped, Line, Map, Pos};

    static TEST_MAP: Lazy<Map> = Lazy::new(|| Map {
        pos_min: Pos { x: 7, y: 42 },
        pos_max: Pos { x: 42, y: 616 },
        heights: [
            Pos { x: 7, y: 10 },
            Pos { x: 8, y: 10 },
            Pos { x: 9, y: 10 },
            Pos { x: 20, y: 10 },
            Pos { x: 21, y: 10 },
            Pos { x: 22, y: 10 },
            Pos { x: 40, y: 10 },
            Pos { x: 41, y: 10 },
            Pos { x: 42, y: 10 },
        ]
        .into_iter()
        .enumerate()
        .map(|(idx, pos)| (pos, (idx & 1) == 0))
        .collect(),
        has_floor: false,
    });

    #[rstest]
    #[case("1,1 -> 1,4\n", &[
        Line { start: Pos { x: 1, y: 1 }, dir: Direction::South, count: 3 },
    ])]
    #[case("1,1 -> 1,4 -> 2,4\n", &[
        Line { start: Pos { x: 1, y: 1 }, dir: Direction::South, count: 3 },
        Line { start: Pos { x: 1, y: 4 }, dir: Direction::East, count: 1 },
    ])]
    #[case("502,9 -> 494,9\n", &[
        Line { start: Pos { x: 502, y: 9 }, dir: Direction::West, count: 8 },
    ])]
    fn test_parse_rock(#[case] line: &str, #[case] expected: &[Line]) -> Result<()> {
        let (r_input, res) = super::p_rock(line)
            .map_err(|err| err.map_input(ToOwned::to_owned))
            .with_context(|| format!("Could not parse the {:?} line", line))?;
        assert_eq!(res, expected);
        assert_eq!(r_input, "");
        Ok(())
    }

    #[rstest]
    #[case(
        "498,5 -> 502,5\n",
        &Map {
            pos_min: Pos { x: 498, y: 5 },
            pos_max: Pos { x: 502, y: 5 },
            heights: [
                (Pos { x: 498, y: 5 }, true),
                (Pos { x: 499, y: 5 }, true),
                (Pos { x: 500, y: 5 }, true),
                (Pos { x: 501, y: 5 }, true),
                (Pos { x: 502, y: 5 }, true),
            ].into(),
            has_floor: false,
        },
    )]
    #[case(
        "502,9 -> 494,9\n",
        &Map {
            pos_min: Pos { x: 494, y: 9 },
            pos_max: Pos { x: 502, y: 9 },
            heights: [
                (Pos { x: 494, y: 9 }, true),
                (Pos { x: 495, y: 9 }, true),
                (Pos { x: 496, y: 9 }, true),
                (Pos { x: 497, y: 9 }, true),
                (Pos { x: 498, y: 9 }, true),
                (Pos { x: 499, y: 9 }, true),
                (Pos { x: 500, y: 9 }, true),
                (Pos { x: 501, y: 9 }, true),
                (Pos { x: 502, y: 9 }, true),
            ].into(),
            has_floor: false,
        },
    )]
    fn test_parse_rocks(#[case] line: &str, #[case] expected: &Map) -> Result<()> {
        assert_eq!(super::parse_rocks(line)?, *expected);
        Ok(())
    }

    #[rstest]
    #[case(Pos { x: 20, y: 9 }, &Dropped::Move(Pos { x: 19, y: 10 }))]
    #[case(Pos { x: 21, y: 9 }, &Dropped::Rest)]
    #[case(Pos { x: 22, y: 9 }, &Dropped::Move(Pos { x: 23, y: 10 }))]
    #[case(Pos { x: 7, y: 9 }, &Dropped::Escape)]
    #[case(Pos { x: 8, y: 9 }, &Dropped::Rest)]
    #[case(Pos { x: 41, y: 9 }, &Dropped::Rest)]
    #[case(Pos { x: 42, y: 9 }, &Dropped::Escape)]
    #[traced_test]
    fn test_drop_sand_single(#[case] pos: Pos, #[case] expected: &Dropped) -> Result<()> {
        let map = &TEST_MAP;
        assert_eq!(map.drop_sand_single(&pos)?, *expected);
        Ok(())
    }
}
