/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Parse the locator signal.

use std::collections::{HashSet, VecDeque};
use std::fs;

use anyhow::Context;

use crate::defs::{Config, Error};

/// A ring buffer of characters.
struct Ring {
    /// The ring buffer itself.
    buf: VecDeque<char>,

    /// The length of the ring buffer.
    count: usize,
}

impl Ring {
    /// Add a character, check whether they are all different now.
    fn handle(&mut self, sig: char) -> bool {
        self.buf.push_back(sig);
        if self.buf.len() < self.count {
            return false;
        }
        if self.buf.len() > self.count {
            self.buf.pop_front();
        }
        self.buf.iter().collect::<HashSet<_>>().len() == self.count
    }
}

/// Push things around.
fn do_it<C: Config>(cfg: &C, count: usize) -> Result<String, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    Ok(contents
        .lines()
        .map(|line| -> Result<usize, Error> {
            let mut ring = Ring {
                buf: VecDeque::new(),
                count,
            };
            line.chars()
                .enumerate()
                .find_map(|(idx, sig)| ring.handle(sig).then_some(idx))
                .context("Nothing found in an input line")
                .map_err(Error::Value)
        })
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .map(|value| {
            Ok(value
                .checked_add(1)
                .context("Could not add 1 to the index?!")
                .map_err(Error::Value)?
                .to_string())
        })
        .collect::<Result<Vec<_>, _>>()?
        .join(" "))
}

/// Figure out where the first four different characters occur.
///
/// # Errors
/// [`Error::Load`] on filesystem or parse errors.
/// [`Error::Value`] on inconsistent input data.
#[inline]
pub fn test_06_1<C: Config>(cfg: &C) -> Result<String, Error> {
    do_it(cfg, 4)
}

/// Figure out where the first four different characters occur.
///
/// # Errors
/// [`Error::Load`] on filesystem or parse errors.
/// [`Error::Value`] on inconsistent input data.
#[inline]
pub fn test_06_2<C: Config>(cfg: &C) -> Result<String, Error> {
    do_it(cfg, 14)
}
