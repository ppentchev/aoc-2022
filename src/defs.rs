/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Common definitions for the Advent of Code 2022 solvers.

use std::collections::HashMap;
use std::fmt::{Debug, Display};
use std::path::Path;

use anyhow::{anyhow, Context, Error as AnyError};
use thiserror::Error;

/// Errors that occur during processing stuff.
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    /// Could not load an input file.
    #[error("Could not load input data from the {0} file")]
    Load(String, #[source] AnyError),

    /// Something went really, really wrong...
    #[error("aoc-2022 internal error")]
    Internal(#[source] AnyError),

    /// Something went wrong during processing the data.
    #[error("Could not process the input data")]
    Process(#[source] AnyError),

    /// Unexpected data in the input files.
    #[error("Unexpected input data")]
    Value(#[source] AnyError),
}

/// Common configuration for the tests.
pub trait Config: Debug {
    /// The input filename to process.
    fn filename(&self) -> &Path;
}

// Common structures used in various tasks.

/// The representation of a movement direction.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
// OVERRIDE: Do we really expect any more directions to pop up?
#[allow(clippy::exhaustive_enums)]
pub enum Direction {
    /// Go north.
    North,

    /// Go northeast.
    Northeast,

    /// Go east.
    East,

    /// Go southeast.
    Southeast,

    /// Go south.
    South,

    /// Go southwest.
    Southwest,

    /// Go west.
    West,

    /// Go northwest.
    Northwest,
}

impl Direction {
    /// Get the (x, y) delta for a single step.
    #[inline]
    #[must_use]
    // OVERRIDE: Eh, I'd say it's pretty obvious what is going on here, right?
    #[allow(clippy::default_numeric_fallback)]
    pub const fn step(&self) -> (i32, i32) {
        match *self {
            Self::North => (0, -1),
            Self::Northeast => (1, -1),
            Self::East => (1, 0),
            Self::Southeast => (1, 1),
            Self::South => (0, 1),
            Self::Southwest => (-1, 1),
            Self::West => (-1, 0),
            Self::Northwest => (-1, -1),
        }
    }

    /// Get a direction from a (x, y) delta.
    #[inline]
    #[must_use]
    pub fn from_xy(dx: i32, dy: i32) -> Self {
        // OK, so this is kind of lame, but we want this function to be const,
        // so we cannot really use [`std::cmp::Ord::cmp`] and `std::cmp::Ordering` here.
        #[allow(clippy::comparison_chain)]
        #[allow(clippy::default_numeric_fallback)]
        // OVERRIDE: yes, the (0, 0) case should really never happen. Honest.
        #[allow(clippy::unreachable)]
        if dx < 0 {
            if dy < 0 {
                Self::Northwest
            } else if dy == 0 {
                Self::West
            } else {
                Self::Southwest
            }
        } else if dx == 0 {
            if dy < 0 {
                Self::North
            } else if dy > 0 {
                Self::South
            } else {
                unreachable!()
            }
        } else if dy < 0 {
            Self::Northeast
        } else if dy == 0 {
            Self::East
        } else {
            Self::Southeast
        }
    }
}

/// A type (usually an integer one) that implements `.checked_add()` and `.checked_sub()`.
pub trait CheckedAddSub: Sized {
    /// Add to the current value.
    fn checked_add(self, rhs: Self) -> Option<Self>;

    /// Subtract from the current value.
    fn checked_sub(self, rhs: Self) -> Option<Self>;
}

impl CheckedAddSub for i32 {
    #[inline]
    // OVERRIDE: Nope, we don't want recursion here, thank you very much.
    #[allow(clippy::use_self)]
    fn checked_add(self, rhs: Self) -> Option<Self> {
        i32::checked_add(self, rhs)
    }

    #[inline]
    // OVERRIDE: Nope, we don't want recursion here, thank you very much.
    #[allow(clippy::use_self)]
    fn checked_sub(self, rhs: Self) -> Option<Self> {
        i32::checked_sub(self, rhs)
    }
}

impl CheckedAddSub for u32 {
    #[inline]
    // OVERRIDE: Nope, we don't want recursion here, thank you very much.
    #[allow(clippy::use_self)]
    fn checked_add(self, rhs: Self) -> Option<Self> {
        u32::checked_add(self, rhs)
    }

    #[inline]
    // OVERRIDE: Nope, we don't want recursion here, thank you very much.
    #[allow(clippy::use_self)]
    fn checked_sub(self, rhs: Self) -> Option<Self> {
        u32::checked_sub(self, rhs)
    }
}

impl CheckedAddSub for usize {
    #[inline]
    // OVERRIDE: Nope, we don't want recursion here, thank you very much.
    #[allow(clippy::use_self)]
    fn checked_add(self, rhs: Self) -> Option<Self> {
        usize::checked_add(self, rhs)
    }

    #[inline]
    // OVERRIDE: Nope, we don't want recursion here, thank you very much.
    #[allow(clippy::use_self)]
    fn checked_sub(self, rhs: Self) -> Option<Self> {
        usize::checked_sub(self, rhs)
    }
}

/// Provide the constant "1" for the specified type.
pub trait ConstOne {
    /// This is the constant "1".
    fn const_1() -> Self;
}

impl ConstOne for i32 {
    #[inline]
    fn const_1() -> Self {
        1_i32
    }
}

impl ConstOne for u32 {
    #[inline]
    fn const_1() -> Self {
        1_u32
    }
}

impl ConstOne for usize {
    #[inline]
    fn const_1() -> Self {
        1_usize
    }
}

/// A (x, y) position on a map.
pub trait PosXY: Copy + Clone {
    /// The coordinates type.
    type Coord: Copy + Display + CheckedAddSub + ConstOne;

    /// Get the X coordinate.
    fn x(&self) -> Self::Coord;

    /// Get the Y coordinate.
    fn y(&self) -> Self::Coord;

    /// Build a position from two coordinates.
    fn from_xy(x: Self::Coord, y: Self::Coord) -> Self;

    /// Build a position from a tuple of coordinates.
    #[inline]
    #[must_use]
    fn from_tuple(xy: (Self::Coord, Self::Coord)) -> Self {
        Self::from_xy(xy.0, xy.1)
    }

    /// Return a new position with updated coordinates.
    ///
    /// # Errors
    /// [`Error::Process`] on integer overflow.
    #[inline]
    fn step(&self, dir: Direction) -> Result<Self, Error> {
        let do_dec = |value: Self::Coord| {
            value
                .checked_sub(Self::Coord::const_1())
                .with_context(|| format!("Could not subtract 1 from {}", value))
                .map_err(Error::Process)
        };
        let do_inc = |value: Self::Coord| {
            value
                .checked_add(Self::Coord::const_1())
                .with_context(|| format!("Could not add 1 to {}", value))
                .map_err(Error::Process)
        };

        let (x, y) = match dir {
            Direction::North => (self.x(), do_dec(self.y())?),
            Direction::Northeast => (do_inc(self.x())?, do_dec(self.y())?),
            Direction::East => (do_inc(self.x())?, self.y()),
            Direction::Southeast => (do_inc(self.x())?, do_inc(self.y())?),
            Direction::South => (self.x(), do_inc(self.y())?),
            Direction::Southwest => (do_dec(self.x())?, do_inc(self.y())?),
            Direction::West => (do_dec(self.x())?, self.y()),
            Direction::Northwest => (do_dec(self.x())?, do_dec(self.y())?),
        };
        Ok(Self::from_xy(x, y))
    }

    /// Return a new position with updated coordinates.
    ///
    /// # Errors
    /// [`Error::Process`] on integer overflow.
    #[inline]
    fn step_xy(&self, dx: i32, dy: i32) -> Result<Self, Error> {
        #[allow(clippy::default_numeric_fallback)]
        match (dx, dy) {
            (0, -1) => self.step(Direction::North),
            (1, -1) => self.step(Direction::Northeast),
            (1, 0) => self.step(Direction::East),
            (1, 1) => self.step(Direction::Southeast),
            (0, 1) => self.step(Direction::South),
            (-1, 1) => self.step(Direction::Southwest),
            (-1, 0) => self.step(Direction::West),
            (-1, -1) => self.step(Direction::Northwest),
            (0, 0) => Ok(*self),
            (o_dx, o_dy) => Err(Error::Internal(anyhow!(format!(
                "Unexpected (dx, dy) pair passed to step_xy(): ({}, {})",
                o_dx, o_dy
            )))),
        }
    }
}

/// A representation of the map read from the input file.
pub trait MapXY<P: PosXY, T> {
    /// Get the map size.
    fn size(&self) -> P;

    /// Get the start position for a route on the map.
    fn start(&self) -> P;

    /// Get the end position for a route on the map.
    fn end(&self) -> P;

    /// Get a reference to the position -> value hashmap.
    fn heights(&self) -> &HashMap<P, T>;
}
