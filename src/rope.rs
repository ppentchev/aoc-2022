/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Let the rope flail all around.

use std::collections::HashSet;
use std::fs;
use std::iter;

use anyhow::{anyhow, Context};
use nom::{
    bytes::complete::tag,
    character::complete::{one_of, u32 as p_u32},
    combinator::all_consuming,
    sequence::{pair, preceded},
    IResult,
};
use tracing::trace;

use crate::defs::{Config, Direction, Error, PosXY};

/// The representation of a single position.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Pos(i32, i32);

impl PosXY for Pos {
    type Coord = i32;

    /// Initialize a position from two coordinates.
    #[inline]
    #[must_use]
    fn from_xy(x: i32, y: i32) -> Self {
        Self(x, y)
    }

    /// Get the X coordinate.
    #[inline]
    #[must_use]
    fn x(&self) -> i32 {
        self.0
    }

    /// Get the Y coordinate.
    #[inline]
    #[must_use]
    fn y(&self) -> i32 {
        self.1
    }
}

impl Pos {
    /// Get the midpoint if `abs(ours - other) == 2`.
    ///
    /// # Errors
    /// [`Error::Internal`] if the arithmetic does not happen quite as we expect.
    fn midpoint(ours: i32, other: i32) -> Result<i32, Error> {
        if ours < other {
            Ok(ours
                .checked_add(1)
                .with_context(|| format!("Expected to be able to add 1 to {}", ours))
                .map_err(Error::Internal)?)
        } else {
            Ok(ours
                .checked_sub(1)
                .with_context(|| format!("Expected to be able to subtract 1 from {}", ours))
                .map_err(Error::Internal)?)
        }
    }

    /// Make a step closer when we are in the same row or column.
    ///
    /// # Errors
    /// [`Error::Internal`] if the arithmetic does not happen quite as we expect.
    fn step_closer_same(ours: i32, other: i32) -> Result<i32, Error> {
        match ours.abs_diff(other) {
            0 | 1 => Ok(ours),
            2 => Self::midpoint(ours, other),
            _ => Err(Error::Internal(anyhow!(format!(
                "{} is too far away from {}, how did this happen?",
                ours, other
            )))),
        }
    }

    /// Return a new `Pos` object that moves closer to the other one.
    ///
    /// # Errors
    /// whee?
    #[inline]
    pub fn step_closer(&self, other: &Self) -> Result<Self, Error> {
        if self.x() == other.x() {
            Ok(Self(self.x(), Self::step_closer_same(self.y(), other.y())?))
        } else if self.y() == other.y() {
            Ok(Self(Self::step_closer_same(self.x(), other.x())?, self.y()))
        } else {
            match (self.x().abs_diff(other.x()), self.y().abs_diff(other.y())) {
                (1, 1) => Ok(*self),
                (1, 2) => Ok(Self(other.x(), Self::midpoint(self.y(), other.y())?)),
                (2, 1) => Ok(Self(Self::midpoint(self.x(), other.x())?, other.y())),
                (2, 2) => Ok(Self(
                    Self::midpoint(self.x(), other.x())?,
                    Self::midpoint(self.y(), other.y())?,
                )),
                _ => Err(Error::Value(anyhow!(format!(
                    "({}, {}) is too far away from ({}, {})",
                    self.x(),
                    self.y(),
                    other.x(),
                    other.y()
                )))),
            }
        }
    }
}

/// The representation of the current state of the rope.
#[derive(Debug)]
pub struct Rope {
    /// The coordinates of the head of the rope.
    head: Pos,

    /// The coordinates of the tail of the rope.
    tail: Pos,
}

impl Rope {
    /// Define a new rope.
    #[inline]
    #[must_use]
    pub const fn new(head: Pos, tail: Pos) -> Self {
        Self { head, tail }
    }

    /// This is the head of the rope.
    #[inline]
    #[must_use]
    pub const fn head(&self) -> Pos {
        self.head
    }

    /// This is the tail of the rope.
    #[inline]
    #[must_use]
    pub const fn tail(&self) -> Pos {
        self.tail
    }

    /// Move the head, let the tail chase it around.
    ///
    /// # Errors
    /// [`Error::Value`] on input that would lead to an integer overflow.
    #[inline]
    pub fn move_head(&self, dx: i32, dy: i32) -> Result<Self, Error> {
        let new_head = self
            .head
            .step_xy(dx, dy)
            .context("Could not move the head")
            .map_err(Error::Value)?;
        let new_tail = self
            .tail
            .step_closer(&new_head)
            .context("Could not move the tail")
            .map_err(Error::Internal)?;
        Ok(Self {
            head: new_head,
            tail: new_tail,
        })
    }
}

/// A longer rope.
#[derive(Debug)]
struct LongRope {
    /// The current positions of all the knots, from the head to the tail.
    knots: Vec<Pos>,
}

impl LongRope {
    /// Construct a rope that starts with all knots at the specified position.
    #[inline]
    #[must_use]
    pub fn new(pos: Pos, count: usize) -> Self {
        assert!(count != 0, "Empty rope not allowed");
        Self {
            knots: iter::repeat(pos).take(count).collect(),
        }
    }

    /// Get the position of the tail end of the rope.
    #[inline]
    #[must_use]
    pub fn tail(&self) -> Pos {
        // OVERRIDE: The constructor said we can do that.
        #[allow(clippy::unwrap_used)]
        *self.knots.last().unwrap()
    }

    /// Make sure each knot is close to the previous one.
    #[inline]
    pub fn validate(&self) -> Result<(), Error> {
        let mut knots = self.knots.iter();
        let head = knots
            .next()
            .context("No knots at all?!")
            .map_err(Error::Internal)?;
        knots.try_fold(head, |prev, next| {
            if prev.x().abs_diff(next.x()) > 1 {
                Err(Error::Internal(anyhow!(format!(
                    "The X coordinates of {:?} and {:?} differ too much",
                    prev, next
                ))))
            } else if prev.y().abs_diff(next.y()) > 1 {
                Err(Error::Internal(anyhow!(format!(
                    "The Y coordinates of {:?} and {:?} differ too much",
                    prev, next
                ))))
            } else {
                Ok(next)
            }
        })?;
        Ok(())
    }

    /// Move the head, let the rest of the knots follow.
    ///
    /// # Errors
    /// [`Error::Value`] on integer overflow.
    #[inline]
    pub fn move_head(&mut self, dx: i32, dy: i32) -> Result<Self, Error> {
        let mut knots = self.knots.iter();
        let first = knots
            .next()
            .context("No knots at all?!")
            .map_err(Error::Internal)?;
        let (tail, last_horiz, last_vert, mut acc) = knots.try_fold(
            (*first, dx, dy, Vec::new()),
            |(head, cur_dx, cur_dy, mut acc), tail| {
                let try_sub = |upd: i32, orig: i32| {
                    upd.checked_sub(orig)
                        .with_context(|| format!("Could not subtract {} from {}", orig, upd))
                        .map_err(Error::Internal)
                };
                let start = Rope { head, tail: *tail };
                trace!("start {:?} dx {} dy {}", start, cur_dx, cur_dy);
                let updated = start.move_head(cur_dx, cur_dy)?;
                trace!(?updated);
                acc.push(updated.head);
                trace!(?acc);
                Ok((
                    start.tail,
                    try_sub(updated.tail.x(), start.tail.x())?,
                    try_sub(updated.tail.y(), start.tail.y())?,
                    acc,
                ))
            },
        )?;
        acc.push(tail.step_xy(last_horiz, last_vert)?);

        let res = Self { knots: acc };
        res.validate()
            .with_context(|| {
                format!(
                    "Tried to step ({}, {}) from {:?}, got invalid {:?}",
                    dx, dy, self.knots, res.knots
                )
            })
            .map_err(Error::Internal)?;
        Ok(res)
    }
}

/// Parse a direction character.
// OVERRIDE: We can't fix IResult.
#[allow(clippy::panic_in_result_fn)]
#[allow(clippy::unreachable)]
fn p_direction(input: &str) -> IResult<&str, Direction> {
    let (r_input, dir) = one_of("UDLR")(input)?;
    Ok((
        r_input,
        match dir {
            'U' => Direction::North,
            'D' => Direction::South,
            'L' => Direction::West,
            'R' => Direction::East,
            _ => unreachable!(),
        },
    ))
}

/// Parse a full 'U 15' line.
fn p_step(input: &str) -> IResult<&str, (Direction, usize)> {
    let (r_input, (dir, count)) =
        all_consuming(pair(p_direction, preceded(tag(" "), p_u32)))(input)?;
    // OVERRIDE: We really hope a u32 would fit into an usize.
    #[allow(clippy::unwrap_used)]
    Ok((r_input, (dir, usize::try_from(count).unwrap())))
}

/// Let the rope move around and stuff.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
#[inline]
pub fn test_09_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let steps = {
        let contents = fs::read_to_string(cfg.filename())
            .context("Could not read the input file")
            .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
        contents
            .lines()
            .map(|line| {
                p_step(line)
                    .map(|(_, (dir, count))| (dir, count))
                    .map_err(|err| err.map_input(ToOwned::to_owned))
                    .with_context(|| {
                        format!("Could not parse an input line: '{}'", line.escape_debug())
                    })
                    .map_err(Error::Value)
            })
            .collect::<Result<Vec<_>, _>>()?
    };
    let res = steps.into_iter().try_fold(
        (Rope::new(Pos(0, 0), Pos(0, 0)), HashSet::from([Pos(0, 0)])),
        |(mut current, mut seen), (dir, count)| {
            for _ in 0..count {
                let (dx, dy) = dir.step();
                current = current.move_head(dx, dy)?;
                seen.insert(current.tail);
            }
            Ok((current, seen))
        },
    )?;
    Ok(res.1.len().to_string())
}

/// Let the long rope move around and stuff.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
#[inline]
pub fn test_09_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let steps = {
        let contents = fs::read_to_string(cfg.filename())
            .context("Could not read the input file")
            .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
        contents
            .lines()
            .map(|line| {
                p_step(line)
                    .map(|(_, (dir, count))| (dir, count))
                    .map_err(|err| err.map_input(ToOwned::to_owned))
                    .with_context(|| {
                        format!("Could not parse an input line: '{}'", line.escape_debug())
                    })
                    .map_err(Error::Value)
            })
            .collect::<Result<Vec<_>, _>>()?
    };
    let res = steps.into_iter().try_fold(
        (LongRope::new(Pos(0, 0), 10), HashSet::from([Pos(0, 0)])),
        |(mut current, mut seen), (dir, count)| {
            for _ in 0..count {
                let (dx, dy) = dir.step();
                current = current.move_head(dx, dy)?;
                seen.insert(current.tail());
            }
            Ok((current, seen))
        },
    )?;
    Ok(res.1.len().to_string())
}

#[cfg(test)]
// OVERRIDE: This is a test suite.
#[allow(clippy::panic_in_result_fn)]
#[allow(clippy::default_numeric_fallback)]
mod tests {
    use anyhow::{Context, Result};
    use rstest::rstest;
    use tracing_test::traced_test;

    use super::{LongRope, Pos, Rope};

    #[rstest]
    #[case(Pos(2, 1), Pos(1, 1), 1, 0, Pos(3, 1), Pos(2, 1))]
    #[case(Pos(1, 2), Pos(1, 1), 0, 1, Pos(1, 3), Pos(1, 2))]
    #[case(Pos(1, 1), Pos(2, 1), -1, 0, Pos(0, 1), Pos(1, 1))]
    #[case(Pos(1, 1), Pos(1, 2), 0, -1, Pos(1, 0), Pos(1, 1))]
    #[case(Pos(3, 4), Pos(3, 5), 0, 0, Pos(3, 4), Pos(3, 5))]
    #[case(Pos(4, 3), Pos(5, 3), 0, 0, Pos(4, 3), Pos(5, 3))]
    #[case(Pos(3, 4), Pos(2, 4), 0, 0, Pos(3, 4), Pos(2, 4))]
    #[case(Pos(4, 3), Pos(4, 2), 0, 0, Pos(4, 3), Pos(4, 2))]
    #[case(Pos(2, 5), Pos(2, 5), 0, 0, Pos(2, 5), Pos(2, 5))]
    #[case(Pos(2, 5), Pos(2, 5), 1, 0, Pos(3, 5), Pos(2, 5))]
    #[case(Pos(2, 5), Pos(2, 5), 0, 1, Pos(2, 6), Pos(2, 5))]
    #[case(Pos(2, 5), Pos(2, 5), -1, 0, Pos(1, 5), Pos(2, 5))]
    #[case(Pos(2, 5), Pos(2, 5), 0, -1, Pos(2, 4), Pos(2, 5))]
    #[case(Pos(2, 2), Pos(1, 3), 0, -1, Pos(2, 1), Pos(2, 2))]
    #[case(Pos(2, 2), Pos(1, 3), 1, 0, Pos(3, 2), Pos(2, 2))]
    #[case(Pos(1, 1), Pos(2, 2), 0, 0, Pos(1, 1), Pos(2, 2))]
    #[case(Pos(2, 1), Pos(1, 1), 0, 1, Pos(2, 2), Pos(1, 1))]
    fn test_rope_move(
        #[case] start_head: Pos,
        #[case] start_tail: Pos,
        #[case] dx: i32,
        #[case] dy: i32,
        #[case] expected_head: Pos,
        #[case] expected_tail: Pos,
    ) -> Result<()> {
        let current = Rope::new(start_head, start_tail);
        assert_eq!((current.head(), current.tail()), (start_head, start_tail));
        let updated = current
            .move_head(dx, dy)
            .context("Could not move the rope")?;
        assert_eq!(
            (updated.head(), updated.tail()),
            (expected_head, expected_tail)
        );
        Ok(())
    }

    #[rstest]
    #[case(
        [Pos(3, 1), Pos(2, 0), Pos(3, 1), Pos(3, 2), Pos(2, 2)],
        1, 0,
        [Pos(4, 1), Pos(3, 1), Pos(3, 1), Pos(3, 2), Pos(2, 2)]
    )]
    #[traced_test]
    fn test_long_rope_move(
        #[case] start: [Pos; 5],
        #[case] dx: i32,
        #[case] dy: i32,
        #[case] expected: [Pos; 5],
    ) -> Result<()> {
        let mut current = LongRope {
            knots: start.into(),
        };
        let updated = current
            .move_head(dx, dy)
            .context("Could not move the long rope")?;
        assert_eq!(updated.knots, expected);
        Ok(())
    }
}
