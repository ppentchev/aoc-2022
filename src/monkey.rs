/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Monkey business.

use std::fs;

use anyhow::{anyhow, Context};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{u32 as p_u32, u64 as p_u64},
    combinator::all_consuming,
    multi::separated_list1,
    sequence::{delimited, preceded, tuple},
    IResult,
};
use tracing::trace;

use crate::defs::{Config, Error};

/// What a monkey does after getting bored with an item.
#[derive(Debug)]
struct Action {
    /// Which monkey do we throw the item to.
    target: usize,

    /// Which item do we throw.
    item: u64,
}

/// Worry change as a monkey inspects an item.
#[derive(Debug)]
enum Inspect {
    /// The worrly level is increased by the specified amount.
    Add(u64),

    /// The worrly level multiplies by the specified amount.
    Mul(u64),

    /// The worrly level is squared.
    Squared,
}

impl Inspect {
    /// Apply the specified action to an item's worry level.
    fn apply(&self, value: u64) -> Result<u64, Error> {
        match *self {
            Self::Add(more) => value
                .checked_add(more)
                .with_context(|| format!("Could not add {} to {}", more, value))
                .map_err(Error::Process),
            Self::Mul(times) => value
                .checked_mul(times)
                .with_context(|| format!("Could not multiply {} by {}", value, times))
                .map_err(Error::Process),
            Self::Squared => value
                .checked_mul(value)
                .with_context(|| format!("Could not compute {} squared", value))
                .map_err(Error::Process),
        }
    }
}

/// What a monkey does with an item.
#[derive(Debug)]
struct Test {
    /// What the monkey checks.
    divisor: u64,

    /// The recipient if true.
    if_true: usize,

    /// The recipient if false.
    if_false: usize,
}

/// The state of a single monkey.
#[derive(Debug)]
struct Monkey {
    /// What the monkey holds.
    items: Vec<u64>,

    /// What we do while watching the monkey.
    inspect: Inspect,

    /// What the monkey does once it is bored.
    test: Test,

    /// The number of items processed.
    processed: usize,
}

impl Monkey {
    /// Process a single item.
    fn process_item(&self, start: u64, calm_down: bool, threshold: u64) -> Result<Action, Error> {
        let inspected = self
            .inspect
            .apply(start)
            .with_context(|| {
                format!(
                    "Could not inspect the {} item according to the {:?} rule",
                    start, self.inspect
                )
            })
            .map_err(Error::Process)?;
        let unworried = (if calm_down {
            inspected
                .checked_div(3)
                .with_context(|| format!("We should have been able to divide {} by 3", inspected))
                .map_err(Error::Internal)?
        } else {
            inspected
        })
        .checked_rem(threshold)
        .context("Threshold zero?!")
        .map_err(Error::Internal)?;
        if unworried
            .checked_rem(self.test.divisor)
            .with_context(|| {
                format!(
                    "The {} divisor should have been validated already",
                    self.test.divisor
                )
            })
            .map_err(Error::Internal)?
            == 0
        {
            Ok(Action {
                target: self.test.if_true,
                item: unworried,
            })
        } else {
            Ok(Action {
                target: self.test.if_false,
                item: unworried,
            })
        }
    }

    /// Process each item in turn.
    fn process_items(&mut self, calm_down: bool, threshold: u64) -> Result<Vec<Action>, Error> {
        let items: Vec<u64> = { self.items.drain(..).collect() };
        items
            .into_iter()
            .enumerate()
            .map(|(item_idx, item)| {
                self.process_item(item, calm_down, threshold)
                    .with_context(|| format!("Problem processing item number {}", item_idx))
                    .map_err(Error::Process)
            })
            .collect::<Result<_, _>>()
    }

    /// Tally some processed items.
    fn update_processed(&mut self, count: usize) -> Result<(), Error> {
        self.processed = self
            .processed
            .checked_add(count)
            .with_context(|| {
                format!(
                    "Could not record {} more items processed, current {}",
                    count, self.processed
                )
            })
            .map_err(Error::Process)?;
        Ok(())
    }
}

/// Parse a u32 value into a usize one.
fn p_usize(input: &str) -> IResult<&str, usize> {
    let (r_input, value) = p_u32(input)?;
    // OVERRIDE: Let's hope we can fit a u32 value into a usize one.
    #[allow(clippy::unwrap_used)]
    Ok((r_input, usize::try_from(value).unwrap()))
}

/// Parse a monkey's identification line.
fn p_monkey_idx(input: &str) -> IResult<&str, usize> {
    delimited(tag("Monkey "), p_usize, tag(":\n"))(input)
}

/// Parse the list of a monkey's starting items.
fn p_items(input: &str) -> IResult<&str, Vec<u64>> {
    delimited(
        tag("  Starting items: "),
        separated_list1(tag(", "), p_u64),
        tag("\n"),
    )(input)
}

/// Parse the monkey's "add" inspect action.
fn p_inspect_add(input: &str) -> IResult<&str, Inspect> {
    let (r_input, amount) = preceded(tag("+ "), p_u64)(input)?;
    Ok((r_input, Inspect::Add(amount)))
}

/// Parse the monkey's "multiply" inspect action.
fn p_inspect_mul(input: &str) -> IResult<&str, Inspect> {
    let (r_input, amount) = p_u64(input)?;
    Ok((r_input, Inspect::Mul(amount)))
}

/// Parse the monkey's "square" inspect action.
fn p_inspect_squared(input: &str) -> IResult<&str, Inspect> {
    let (r_input, _) = tag("old")(input)?;
    Ok((r_input, Inspect::Squared))
}

/// Parse the monkey's "multiply" or "square" inspect actions.
fn p_inspect_sq_or_mul(input: &str) -> IResult<&str, Inspect> {
    preceded(tag("* "), alt((p_inspect_squared, p_inspect_mul)))(input)
}

/// Parse the monkey's inspect action.
fn p_inspect(input: &str) -> IResult<&str, Inspect> {
    delimited(
        tag("  Operation: new = old "),
        alt((p_inspect_add, p_inspect_sq_or_mul)),
        tag("\n"),
    )(input)
}

/// Parse a monkey's test and further actions.
fn p_test(input: &str) -> IResult<&str, Test> {
    let (r_input, (divisor, if_true, if_false)) = tuple((
        delimited(tag("  Test: divisible by "), p_u64, tag("\n")),
        delimited(tag("    If true: throw to monkey "), p_usize, tag("\n")),
        delimited(tag("    If false: throw to monkey "), p_usize, tag("\n")),
    ))(input)?;
    Ok((
        r_input,
        Test {
            divisor,
            if_true,
            if_false,
        },
    ))
}

/// Parse a single monkey's stuff.
fn p_monkey(input: &str) -> IResult<&str, (usize, Monkey)> {
    let (r_input, (monkey_idx, items, inspect, test)) =
        tuple((p_monkey_idx, p_items, p_inspect, p_test))(input)?;
    Ok((
        r_input,
        (
            monkey_idx,
            Monkey {
                items,
                inspect,
                test,
                processed: 0,
            },
        ),
    ))
}

/// Parse the stuff of the tribe.
fn p_monkeys(input: &str) -> IResult<&str, Vec<(usize, Monkey)>> {
    all_consuming(separated_list1(tag("\n"), p_monkey))(input)
}

/// Parse the stuff of the tribe and validate it.
fn parse_monkeys(input: &str) -> Result<Vec<Monkey>, Error> {
    let (_, raw) = p_monkeys(input)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input data")
        .map_err(Error::Value)?;
    let count = raw.len();
    if count < 2 {
        return Err(Error::Value(anyhow!(format!(
            "At least two monkeys required, only got {}",
            count
        ))));
    }

    raw.into_iter()
        .enumerate()
        .map(|(idx, (monkey_idx, monkey))| {
            if idx != monkey_idx {
                return Err(Error::Value(anyhow!(format!(
                    "Expected a definition for monkey {}, got {} instead: {:?}",
                    idx, monkey_idx, monkey
                ))));
            }
            if monkey.test.divisor == 0 {
                return Err(Error::Value(anyhow!(format!(
                    "Monkey {}: division by zero",
                    idx
                ))));
            }
            if monkey.test.if_true == monkey_idx || monkey.test.if_false == monkey_idx {
                return Err(Error::Value(anyhow!(format!(
                    "Monkey {}: if_true {}, if_false {}",
                    idx, monkey.test.if_true, monkey.test.if_false
                ))));
            }
            if monkey.test.if_true >= count || monkey.test.if_false >= count {
                return Err(Error::Value(anyhow!(format!(
                    "Monkey {}: if_true {}, if_false {}, total monkeys {}",
                    idx, monkey.test.if_true, monkey.test.if_false, count
                ))));
            }
            Ok(monkey)
        })
        .collect::<Result<_, _>>()
}

/// Run a single round over all the monkeys.
fn run_round(monkeys: &mut [Monkey], calm_down: bool, threshold: u64) -> Result<(), Error> {
    (0..monkeys.len()).try_fold(monkeys, |turn_monkeys, monkey_idx| {
        trace!(monkey_idx);
        let monkey_actions = {
            let monkey = turn_monkeys
                .get_mut(monkey_idx)
                .with_context(|| format!("No monkey {}?!", monkey_idx))
                .map_err(Error::Internal)?;
            let actions = monkey
                .process_items(calm_down, threshold)
                .with_context(|| format!("Problem in monkey {}'s turn", monkey_idx))
                .map_err(Error::Process)?;
            monkey
                .update_processed(actions.len())
                .with_context(|| {
                    format!(
                        "Problem updating the processed items for monkey {}",
                        monkey_idx
                    )
                })
                .map_err(Error::Process)?;
            actions
        };
        trace!(?monkey_actions);
        monkey_actions
            .into_iter()
            .try_fold(turn_monkeys, |all_monkeys, action| {
                let monkey = all_monkeys
                    .get_mut(action.target)
                    .with_context(|| {
                        format!("No monkey {} for item {}?!", action.target, action.item)
                    })
                    .map_err(Error::Internal)?;
                monkey.items.push(action.item);
                Ok(all_monkeys)
            })
    })?;
    Ok(())
}

/// Who's an active monkey?
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parse errors or incosistent data.
/// [`Error::Process`] on runtime errors.
fn run<C: Config>(cfg: &C, rounds: usize, calm_down: bool) -> Result<String, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let monkeys = (0..rounds).try_fold(parse_monkeys(&contents)?, |mut monkeys, round_idx| {
        let threshold = monkeys
            .iter()
            .try_fold(1_u64, |acc, monkey| {
                acc.checked_mul(monkey.test.divisor).with_context(|| {
                    format!("Could not multiply {} by {}", acc, monkey.test.divisor)
                })
            })
            .context("Could not compute the division/remainder/threshold value")
            .map_err(Error::Value)?;
        trace!(round_idx);
        run_round(&mut monkeys, calm_down, threshold)
            .with_context(|| format!("Problem in round {}", round_idx))
            .map_err(Error::Process)?;
        Ok(monkeys)
    })?;

    let sorted: Vec<usize> = monkeys
        .into_iter()
        .map(|monkey| monkey.processed)
        .sorted()
        .rev()
        .collect();
    // OVERRIDE: We checked at the start, right?
    #[allow(clippy::indexing_slicing)]
    sorted[0]
        .checked_mul(sorted[1])
        .with_context(|| format!("Could not multiply {} by {}", sorted[0], sorted[1]))
        .map(|value| value.to_string())
        .map_err(Error::Value)
}

/// Run for 20 rounds, get unworried.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parse errors or incosistent data.
/// [`Error::Process`] on runtime errors.
#[inline]
pub fn test_11_1<C: Config>(cfg: &C) -> Result<String, Error> {
    run(cfg, 20, true)
}

/// Run for 10,000 rounds, stay worried.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parse errors or incosistent data.
/// [`Error::Process`] on runtime errors.
#[inline]
pub fn test_11_2<C: Config>(cfg: &C) -> Result<String, Error> {
    run(cfg, 10_000, false)
}
