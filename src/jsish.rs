/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Parse JSON-like lists of (lists of) integers.

use std::cmp::Ordering;
use std::fs;
use std::iter;

use anyhow::Context;
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::u32 as p_u32,
    combinator::all_consuming,
    multi::{separated_list0, separated_list1},
    sequence::{delimited, pair, terminated},
    IResult,
};

use crate::defs::{Config, Error};

/// A single value parsed out of a file.
#[derive(Debug, Clone)]
enum Value {
    /// An integer value.
    Integer(u32),

    /// A list of integers.
    List(Vec<Self>),
}

impl Value {
    /// Compare two lists of values.
    fn cmp_list(mut left: &[Self], mut right: &[Self]) -> Ordering {
        while let Some((left_first, left_rest)) = left.split_first() {
            if let Some((right_first, right_rest)) = right.split_first() {
                let res = left_first.cmp(right_first);
                if res != Ordering::Equal {
                    return res;
                }
                left = left_rest;
                right = right_rest;
            } else {
                return Ordering::Greater;
            }
        }
        if right.is_empty() {
            Ordering::Equal
        } else {
            Ordering::Less
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for Value {}

impl Ord for Value {
    fn cmp(&self, other: &Self) -> Ordering {
        match *self {
            Self::Integer(ref left) => match *other {
                Self::Integer(ref right) => left.cmp(right),
                Self::List(ref right) => Self::cmp_list(&[Self::Integer(*left)], right),
            },
            Self::List(ref left) => match *other {
                Self::Integer(ref right) => Self::cmp_list(left, &[Self::Integer(*right)]),
                Self::List(ref right) => Self::cmp_list(left, right),
            },
        }
    }
}

/// A pair of lists to compare.
#[derive(Debug)]
struct Pair {
    /// The first list of values.
    left: Vec<Value>,

    /// The second list of values.
    right: Vec<Value>,
}

impl Pair {
    /// Is this pair ordered, i.e. is `left` less than or equal to `right`?
    fn is_ordered(&self) -> bool {
        Value::cmp_list(&self.left, &self.right) != Ordering::Greater
    }
}

/// Parse an integer value into a [`Value`] struct.
fn p_value_int(input: &str) -> IResult<&str, Value> {
    let (r_input, value) = p_u32(input)?;
    Ok((r_input, Value::Integer(value)))
}

/// Parse a list of values into a [`Value`] struct.
fn p_value_list(input: &str) -> IResult<&str, Value> {
    let (r_input, value) = p_list(input)?;
    Ok((r_input, Value::List(value)))
}

/// Parse a single value, either an integer or a list.
fn p_value(input: &str) -> IResult<&str, Value> {
    alt((p_value_int, p_value_list))(input)
}

/// Parse a list of values.
fn p_list(input: &str) -> IResult<&str, Vec<Value>> {
    delimited(tag("["), separated_list0(tag(","), p_value), tag("]"))(input)
}

/// Parse a pair of lists of values.
fn p_pair(input: &str) -> IResult<&str, Pair> {
    let (r_input, (left, right)) =
        pair(terminated(p_list, tag("\n")), terminated(p_list, tag("\n")))(input)?;
    Ok((r_input, Pair { left, right }))
}

/// Parse the consecutive pairs of lists from the input file.
fn p_pairs(input: &str) -> IResult<&str, Vec<Pair>> {
    all_consuming(separated_list1(tag("\n"), p_pair))(input)
}

/// Parse the input file into pairs of lists.
fn parse_pairs<C: Config>(cfg: &C) -> Result<Vec<Pair>, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let (_, pairs) = p_pairs(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(Error::Value)?;
    Ok(pairs)
}

/// Figure out which pairs of lists are in the right order.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on inconsistent input data.
/// [`Error::Process`] if the arrays turn out to be too large.
#[inline]
pub fn test_13_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let pairs = parse_pairs(cfg)?;
    let total: usize = pairs
        .into_iter()
        .enumerate()
        .filter_map(|(idx, pair)| {
            pair.is_ordered().then_some(
                idx.checked_add(1)
                    .context("Too many pairs")
                    .map_err(Error::Process),
            )
        })
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .sum();
    Ok(total.to_string())
}

/// Figure something more out.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on inconsistent input data.
/// [`Error::Process`] if the arrays turn out to be too large.
#[inline]
pub fn test_13_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let marker_start = vec![Value::List(vec![Value::Integer(2)])];
    let marker_end = vec![Value::List(vec![Value::Integer(6)])];
    let pairs = {
        let mut parsed = parse_pairs(cfg)?;
        parsed.push(Pair {
            left: marker_start.clone(),
            right: marker_end.clone(),
        });
        parsed
    };
    let lists: Vec<Vec<Value>> = pairs
        .into_iter()
        .flat_map(|pair| iter::once(pair.left).chain(iter::once(pair.right)))
        .sorted()
        .collect();
    let (idx_start, idx_end) = {
        let find_idx = |marker, tag| {
            lists
                .iter()
                .enumerate()
                .find_map(move |(idx, &ref value)| (*value == marker).then_some(idx))
                .with_context(|| format!("Could not find the {} marker in {:?}", tag, lists))
                .map_err(Error::Internal)?
                .checked_add(1)
                .with_context(|| format!("The {} marker's final index is too large", tag))
                .map_err(Error::Process)
        };
        (
            find_idx(marker_start, "start")?,
            find_idx(marker_end, "end")?,
        )
    };
    Ok(idx_start
        .checked_mul(idx_end)
        .with_context(|| format!("Could not multiply {} by {}", idx_start, idx_end))
        .map_err(Error::Process)?
        .to_string())
}
