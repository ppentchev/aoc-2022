/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! I like to move it, move it!

use std::collections::{HashMap, HashSet};
use std::fs;

use anyhow::{anyhow, Context};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{one_of, u32 as p_u32},
    combinator::all_consuming,
    multi::{many1, separated_list1},
    sequence::{delimited, terminated, tuple},
    IResult,
};
use tracing::trace;

use crate::defs::{Config, Error};

/// A line of crates, only the ones that are present.
type StackLine = HashMap<usize, char>;

/// A "take these from here and move them over there" instruction.
#[derive(Debug)]
struct MoveLine {
    /// The column to move crates from.
    src: u32,

    /// The column to move crates to.
    dst: u32,

    /// The number of crates to move.
    count: usize,
}

/// The current state of the stacks of crates.
#[derive(Debug)]
struct Stacks {
    /// The stacks themselves.
    stacks: HashMap<u32, Vec<char>>,
}

impl Stacks {
    /// We like to...
    // OVERRIDE: We can't fix tracing::instrument or IResult
    #[allow(clippy::panic_in_result_fn)]
    #[allow(clippy::unreachable)]
    #[tracing::instrument]
    fn move_it(&mut self, line: &MoveLine, reverse: bool) -> Result<(), Error> {
        trace!(?line);
        let mut moved = {
            let src = self
                .stacks
                .get_mut(&line.src)
                .with_context(|| format!("Invalid 'from' column {}", line.src))
                .map_err(Error::Value)?;
            let left = src
                .len()
                .checked_sub(line.count)
                .with_context(|| {
                    format!(
                        "Not enough crates in the {} stack: want to move {}, only have {}",
                        line.src,
                        line.count,
                        src.len()
                    )
                })
                .map_err(Error::Value)?;
            let moved = {
                let mut rev = src.split_off(left);
                if reverse {
                    rev.reverse();
                }
                rev
            };
            trace!(?src);
            trace!(?moved);
            moved
        };

        {
            let dst = self
                .stacks
                .get_mut(&line.dst)
                .with_context(|| format!("Invalid 'to' column {}", line.src))
                .map_err(Error::Value)?;

            dst.append(&mut moved);
            trace!(?dst);
        }
        Ok(())
    }
}

/// The full set of instructions.
type InstructionSet = (Vec<StackLine>, Vec<u32>, Vec<MoveLine>);

/// Parse a crate in the stacking diagram.
fn p_stack_crate(input: &str) -> IResult<&str, Option<char>> {
    let (r_input, res) =
        delimited(tag("["), one_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), tag("]"))(input)?;
    Ok((r_input, Some(res)))
}

/// Parse an empty space in the stacking diagram.
fn p_stack_empty(input: &str) -> IResult<&str, Option<char>> {
    let (r_input, _) = tag("   ")(input)?;
    Ok((r_input, None))
}

/// Parse a single stack line.
fn p_stack_line(input: &str) -> IResult<&str, StackLine> {
    let (r_input, parts) = terminated(
        separated_list1(tag(" "), alt((p_stack_crate, p_stack_empty))),
        tag("\n"),
    )(input)?;
    let res: HashMap<_, _> = parts
        .into_iter()
        .enumerate()
        .filter_map(|(idx, item)| item.map(|value| (idx, value)))
        .collect();
    Ok((r_input, res))
}

/// Parse the line with the crate column labels and the empty line thereafter.
fn p_labels_line(input: &str) -> IResult<&str, Vec<u32>> {
    terminated(
        separated_list1(tag(" "), delimited(tag(" "), p_u32, tag(" "))),
        tag("\n\n"),
    )(input)
}

/// Parse a "I like to move it" instruction line.
fn p_move_line(input: &str) -> IResult<&str, MoveLine> {
    let (r_input, (_, count, _, src, _, dst)) = terminated(
        tuple((
            tag("move "),
            p_u32,
            tag(" from "),
            p_u32,
            tag(" to "),
            p_u32,
        )),
        tag("\n"),
    )(input)?;
    // OVERRIDE: We hope usize can hold u32.
    #[allow(clippy::unwrap_used)]
    Ok((
        r_input,
        MoveLine {
            src,
            dst,
            count: count.try_into().unwrap(),
        },
    ))
}

/// Parse the full set of instructions for moving crates around.
fn p_stack_move(input: &str) -> IResult<&str, InstructionSet> {
    all_consuming(tuple((
        many1(p_stack_line),
        p_labels_line,
        many1(p_move_line),
    )))(input)
}

/// Transpose the stack lines into a series of arrays.
fn build_stack(stack_lines: Vec<StackLine>, labels_line: &[u32]) -> Result<Stacks, Error> {
    if stack_lines
        .iter()
        .any(|line| line.len() > labels_line.len())
    {
        return Err(Error::Value(anyhow!("Not enough labels")));
    }
    let reversed: HashMap<_, _> =
        stack_lines
            .into_iter()
            .try_fold(HashMap::new(), |mut acc, line| {
                if !line
                    .keys()
                    .collect::<HashSet<_>>()
                    .is_superset(&acc.keys().collect::<HashSet<_>>())
                {
                    return Err(Error::Value(anyhow!(format!(
                    "Next line crate positions {} do not cover the positions collected so far {}",
                    line.keys().sorted_unstable().join(","),
                    acc.keys().sorted_unstable().join(",")
                ))));
                }
                Ok(line
                    .into_iter()
                    .map(|(pos, new_crate)| {
                        let mut stack: Vec<_> = acc.remove(&pos).unwrap_or_default();
                        stack.push(new_crate);
                        (pos, stack)
                    })
                    .collect())
            })?;
    Ok(Stacks {
        stacks: reversed
            .into_iter()
            .map(|(pos, mut stack)| {
                stack.reverse();
                // OVERRIDE: We checked at the start of the function.
                #[allow(clippy::indexing_slicing)]
                (labels_line[pos], stack)
            })
            .collect(),
    })
}

/// So who's on top, anyway?
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
/// [`Error::Value`] on inconsistent instructions given in the input file.
fn do_it<C: Config>(cfg: &C, reverse: bool) -> Result<String, Error> {
    let load_err = |err| Error::Load(format!("{}", cfg.filename().display()), err);
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(load_err)?;
    let (_, (stack_lines, labels_line, move_lines)) = p_stack_move(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(load_err)?;
    let mut stacks = build_stack(stack_lines, &labels_line)?;

    for line in &move_lines {
        stacks.move_it(line, reverse)?;
    }

    trace!(?stacks);
    Ok(stacks
        .stacks
        .iter()
        .sorted()
        .map(|(_, stack)| {
            stack
                .last()
                .context("Nothing left on a stack at the end")
                .map_err(Error::Value)
        })
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .copied()
        .join(""))
}

/// Who's on top?
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
/// [`Error::Value`] on inconsistent instructions given in the input file.
#[inline]
pub fn test_05_1<C: Config>(cfg: &C) -> Result<String, Error> {
    do_it(cfg, true)
}

/// Who's on top, but with reversed moves?
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
/// [`Error::Value`] on inconsistent instructions given in the input file.
#[inline]
pub fn test_05_2<C: Config>(cfg: &C) -> Result<String, Error> {
    do_it(cfg, false)
}
