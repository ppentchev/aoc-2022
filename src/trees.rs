/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! It's full of trees!

use std::fs;

use anyhow::{anyhow, Context};
use tracing::trace;

use crate::defs::{Config, Error};

/// Read the tree map from the input file.
fn parse_trees<C: Config>(cfg: &C) -> Result<(Vec<Vec<u32>>, usize, usize), Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let lines: Vec<_> = contents.lines().collect();
    let width = lines
        .first()
        .context("Not even a single line?")
        .map_err(Error::Value)?
        .len();
    let height = lines.len();

    let trees: Vec<Vec<u32>> = lines
        .into_iter()
        .map(|line| {
            line.chars()
                .map(|digit| {
                    digit
                        .to_digit(10)
                        .with_context(|| format!("Invalid digit: '{}'", digit.escape_debug()))
                        .map_err(Error::Value)
                })
                .collect::<Result<_, _>>()
        })
        .collect::<Result<_, _>>()?;

    if trees.iter().any(|row| row.len() != width) {
        return Err(Error::Value(anyhow!(format!(
            "Not all tree rows are {} trees wide",
            width
        ))));
    }
    Ok((trees, width, height))
}

/// Can anybody see me?
// OVERRIDE: we parsed this, right?
#[allow(clippy::indexing_slicing)]
#[allow(clippy::arithmetic_side_effects)]
#[allow(clippy::integer_arithmetic)]
fn is_visible(
    trees: &[Vec<u32>],
    width: usize,
    height: usize,
    row: usize,
    col: usize,
    value: u32,
) -> bool {
    (0..col).all(|x| trees[row][x] < value)
        || ((col + 1)..width).all(|x| trees[row][x] < value)
        || (0..row).all(|y| trees[y][col] < value)
        || ((row + 1)..height).all(|y| trees[y][col] < value)
}

/// Can I see anybody else?
// OVERRIDE: we parsed this, right?
#[allow(clippy::indexing_slicing)]
#[allow(clippy::arithmetic_side_effects)]
#[allow(clippy::integer_arithmetic)]
fn count_seen(
    trees: &[Vec<u32>],
    width: usize,
    height: usize,
    row: usize,
    col: usize,
    value: u32,
) -> usize {
    if row == 0 || row == height - 1 || col == 0 || col == width - 1 {
        return 0;
    }
    trace!("row {} col {}", row, col);
    let mut product: usize = 1;
    product *= {
        let mut subtotal: usize = 0;
        for x in (0..col).rev() {
            subtotal += 1;
            if trees[row][x] >= value {
                break;
            }
        }
        trace!(subtotal);
        subtotal
    };
    product *= {
        let mut subtotal: usize = 0;
        for x in col + 1..width {
            subtotal += 1;
            if trees[row][x] >= value {
                break;
            }
        }
        trace!(subtotal);
        subtotal
    };
    product *= {
        let mut subtotal: usize = 0;
        for y in (0..row).rev() {
            subtotal += 1;
            if trees[y][col] >= value {
                break;
            }
        }
        trace!(subtotal);
        subtotal
    };
    product *= {
        let mut subtotal: usize = 0;
        #[allow(clippy::needless_range_loop)]
        for y in row + 1..height {
            subtotal += 1;
            if trees[y][col] >= value {
                break;
            }
        }
        trace!(subtotal);
        subtotal
    };
    trace!(product);
    product
}

/// How many trees can we see from the outside?
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on an inconsistent trees grid.
#[inline]
pub fn test_08_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let (trees, width, height) = parse_trees(cfg)?;
    let total: usize = trees
        .iter()
        .enumerate()
        .map(|(row, trees_row)| {
            trees_row
                .iter()
                .enumerate()
                .filter(|&(ref col, value)| is_visible(&trees, width, height, row, *col, *value))
                .count()
        })
        .sum();
    Ok(format!("{}", total))
}

/// Now, how many trees can we see from the trees themselves?
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on an inconsistent trees grid.
#[inline]
pub fn test_08_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let (trees, width, height) = parse_trees(cfg)?;
    let highest_per_row: Vec<usize> = trees
        .iter()
        .enumerate()
        .map(|(row, trees_row)| {
            trees_row
                .iter()
                .enumerate()
                .map(|(col, value)| count_seen(&trees, width, height, row, col, *value))
                .max()
                .with_context(|| format!("No max value for row {} {:?}", row, trees_row))
                .map_err(Error::Internal)
        })
        .collect::<Result<_, _>>()?;
    let highest = highest_per_row
        .iter()
        .max()
        .with_context(|| format!("No max value for {:?}", highest_per_row))
        .map_err(Error::Internal)?;
    trace!(?highest);
    Ok(format!("{}", highest))
}
