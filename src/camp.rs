/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Things to do around the elven camp.

use std::fs;
use std::ops::RangeInclusive;

use anyhow::Context;
use nom::{
    bytes::complete::tag,
    character::complete::u32 as p_u32,
    combinator::all_consuming,
    error::{Error as NError, ErrorKind as NErrorKind},
    multi::many1,
    sequence::{separated_pair, terminated},
    Err as NErr, IResult,
};

use crate::defs::{Config, Error};

/// A range of sections assigned to a single elf.
type Sections = RangeInclusive<u32>;

/// Two ranges of sections assigned to a pair of elves.
type Pair = (Sections, Sections);

/// Parse a single range of sections.
fn p_range(input: &str) -> IResult<&str, Sections> {
    let (r_input, (first, second)) = separated_pair(p_u32, tag("-"), p_u32)(input)?;
    if first > second {
        // OVERRIDE: the parser said we can do that.
        #[allow(clippy::unwrap_used)]
        Err(NErr::Failure(NError::new(
            input.strip_suffix(r_input).unwrap(),
            NErrorKind::Fail,
        )))
    } else {
        Ok((r_input, first..=second))
    }
}

/// Parse a pair of section assignments.
fn p_pair(input: &str) -> IResult<&str, Pair> {
    let (r_input, (first, second)) =
        terminated(separated_pair(p_range, tag(","), p_range), tag("\n"))(input)?;
    if first.start() <= second.start() {
        Ok((r_input, (first, second)))
    } else {
        Ok((r_input, (second, first)))
    }
}

/// Parse the list of elven pairs' assignments.
fn p_pairs(input: &str) -> IResult<&str, Vec<Pair>> {
    all_consuming(many1(p_pair))(input)
}

/// Do these pairs overlap fully?
fn overlap_fully(pair: &Pair) -> bool {
    let (ref first, ref second) = *pair;
    // Note that first.start() <= second.start(), since they have been sorted already.
    (first.end() >= second.end()) || (first.start() == second.start() && first.end() < second.end())
}

/// Read pairs of assignments from the input file.
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
fn load_pairs<C: Config>(cfg: &C) -> Result<Vec<Pair>, Error> {
    let load_err = |err| Error::Load(format!("{}", cfg.filename().display()), err);
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(load_err)?;
    let (_, pairs) = p_pairs(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(load_err)?;
    Ok(pairs)
}

/// How many pairs of ranges overlap fully?
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
#[inline]
pub fn test_04_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let pairs = load_pairs(cfg)?;
    Ok(format!(
        "{}",
        pairs.into_iter().filter(overlap_fully).count()
    ))
}

/// How many pairs of ranges overlap in part?
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
#[inline]
pub fn test_04_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let pairs = load_pairs(cfg)?;
    Ok(format!(
        "{}",
        pairs
            .into_iter()
            .filter(|&(ref first, ref second)| first.end() >= second.start())
            .count()
    ))
}

#[cfg(test)]
// OVERRIDE: This is a test suite.
#[allow(clippy::unwrap_used)]
mod tests {
    use rstest::rstest;

    #[rstest]
    #[case("1-3,4-7\n")]
    #[case("1-4,3-8\n")]
    #[case("4-8,1-5\n")]
    #[case("1-6,1-8\n")]
    #[case("2-2,2-2\n")]
    #[case("1-1,7-7\n")]
    #[case("7-7,1-1\n")]
    fn test_parse_pair_ok(#[case] line: &str) {
        let (_, (first, second)) = super::p_pair(line).unwrap();
        assert!(first.start() <= second.start());
    }

    #[rstest]
    #[case("1-34-7\n")]
    #[case("1-4,38\n")]
    #[case("4-8a,1-5\n")]
    #[case("1-6,1-8.\n")]
    #[case("5-1,2-7\n")]
    #[case("1-5,7-2\n")]
    fn test_parse_pair_fail(#[case] line: &str) {
        super::p_pair(line).unwrap_err();
    }
}
