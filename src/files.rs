/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Delve into the locator's directory structure.

use std::collections::{HashMap, HashSet};
use std::fs;

use anyhow::{anyhow, Context};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{one_of, u32 as p_u32},
    combinator::all_consuming,
    multi::many1,
    sequence::{pair, preceded},
    IResult,
};
use tracing::trace;

use crate::defs::{Config, Error};

/// A single line of input.
#[derive(Debug)]
enum Line {
    /// A 'cd <dirname>' command.
    Cd(String),

    /// A 'cd /' command.
    CdRoot,

    /// A 'cd ..' command.
    CdUp,

    /// A 'ls' command.
    Ls,

    /// A directory entry in the 'ls' output.
    OutDir(String),

    /// A file entry in the 'ls' output.
    OutFile(String, u32),
}

/// Parse a 'cd /' command.
fn p_cd_root(input: &str) -> IResult<&str, Line> {
    let (r_input, _) = tag("/")(input)?;
    Ok((r_input, Line::CdRoot))
}

/// Parse a 'cd ..' command.
fn p_cd_up(input: &str) -> IResult<&str, Line> {
    let (r_input, _) = tag("..")(input)?;
    Ok((r_input, Line::CdUp))
}

/// Parse a 'cd <dirname>' command.
fn p_cd_dir(input: &str) -> IResult<&str, Line> {
    let (r_input, name) = many1(one_of("abcdefghijklmnopqrstuvwxyz"))(input)?;
    Ok((r_input, Line::Cd(name.into_iter().collect())))
}

/// Parse a 'cd' command.
fn p_cd(input: &str) -> IResult<&str, Line> {
    preceded(tag("$ cd "), alt((p_cd_root, p_cd_up, p_cd_dir)))(input)
}

/// Parse a 'ls' command.
fn p_ls(input: &str) -> IResult<&str, Line> {
    let (r_input, _) = tag("$ ls")(input)?;
    Ok((r_input, Line::Ls))
}

/// Parse a 'dir <name>' output line.
fn p_out_dir(input: &str) -> IResult<&str, Line> {
    let (r_input, name) =
        preceded(tag("dir "), many1(one_of("abcdefghijklmnopqrstuvwxyz")))(input)?;
    Ok((r_input, Line::OutDir(name.into_iter().collect())))
}

/// Parse a '<size> <filename>' output line.
fn p_out_file(input: &str) -> IResult<&str, Line> {
    let (r_input, (size, name)) = pair(
        p_u32,
        preceded(tag(" "), many1(one_of("abcdefghijklmnopqrstuvwxyz."))),
    )(input)?;
    Ok((r_input, Line::OutFile(name.into_iter().collect(), size)))
}

/// Parse an output line from a 'ls' command.
fn p_out(input: &str) -> IResult<&str, Line> {
    alt((p_out_dir, p_out_file))(input)
}

/// Parse a single line into a [`Line`] item.
fn p_line(input: &str) -> IResult<&str, Line> {
    all_consuming(alt((p_cd, p_ls, p_out)))(input)
}

/// Parse the directory structure.
// OVERRIDE: we cannot fid tracing::instrument
#[allow(clippy::unreachable)]
#[allow(clippy::panic_in_result_fn)]
#[tracing::instrument(skip(lines))]
fn parse_lines<'input, I>(
    cwd_vec: Vec<String>,
    lines: &mut I,
) -> Result<HashMap<String, u32>, Error>
where
    I: Iterator<Item = &'input str>,
{
    let cwd = cwd_vec.join("/");
    let mut res = HashMap::new();
    let mut total: u32 = 0;
    while let Some(line) = lines.next() {
        trace!(line);
        let cmd = p_line(line)
            .map_err(|err| err.map_input(ToOwned::to_owned))
            .with_context(|| format!("Could not parse the '{}' input line", line.escape_debug()))
            .map_err(Error::Value)?
            .1;
        trace!(?cmd);
        match cmd {
            Line::Cd(name) => {
                let new_cwd_vec = {
                    let mut new_vec = cwd_vec.clone();
                    new_vec.push(name.clone());
                    new_vec
                };
                let new_cwd = new_cwd_vec.join("/");
                let mut subres = parse_lines(new_cwd_vec, lines)?;
                trace!(?subres);
                if res
                    .keys()
                    .collect::<HashSet<_>>()
                    .intersection(&subres.keys().collect::<HashSet<_>>())
                    .next()
                    .is_some()
                {
                    return Err(Error::Value(anyhow!(format!(
                        "got {:?} for '{}/{}', we have {:?} so far",
                        subres,
                        cwd.escape_debug(),
                        name.escape_debug(),
                        res
                    ))));
                }
                trace!(total);
                {
                    let subtotal = subres
                        .get(&new_cwd)
                        .with_context(|| {
                            format!("No entry for '{}' in {:?}", new_cwd.escape_debug(), subres)
                        })
                        .map_err(Error::Internal)?;
                    trace!(subtotal);
                    total = total
                        .checked_add(*subtotal)
                        .with_context(|| {
                            format!("Could not add {} to {} for '{}'", subtotal, total, new_cwd)
                        })
                        .map_err(Error::Value)?;
                }
                trace!(total);
                res.extend(subres.drain());
            }
            Line::CdRoot => {
                if !cwd_vec.is_empty() {
                    return Err(Error::Value(anyhow!(format!(
                        "'cd /' in '{}'",
                        cwd.escape_debug()
                    ))));
                }
            }
            Line::CdUp => {
                if cwd_vec.is_empty() {
                    return Err(Error::Value(anyhow!("'cd ..' out of the root directory")));
                }
                break;
            }
            Line::Ls | Line::OutDir(_) => (),
            Line::OutFile(filename, size) => {
                trace!(total);
                trace!(size);
                total = total
                    .checked_add(size)
                    .with_context(|| {
                        format!(
                            "Could not add {} bytes to {} for '{}' in '{}'",
                            size,
                            total,
                            filename.escape_debug(),
                            cwd.escape_debug()
                        )
                    })
                    .map_err(Error::Value)?;
                trace!(total);
            }
        }
    }

    res.insert(cwd, total);
    Ok(res)
}

/// It's over 9000... uh, I mean, under 100000.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parsing or consistency errors.
#[inline]
pub fn test_07_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let dirs = parse_lines(vec![], &mut contents.lines())?;
    trace!(?dirs);
    let total: u32 = dirs.values().filter(|value| **value <= 100_000).sum();
    Ok(format!("{}", total))
}

/// But is it high enough?
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on parsing or consistency errors.
#[inline]
pub fn test_07_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let dirs = parse_lines(vec![], &mut contents.lines())?;
    trace!(?dirs);
    let root_size = dirs
        .get("")
        .with_context(|| format!("No entry for the root directory in {:?}", dirs))
        .map_err(Error::Internal)?;
    let limit = 30_000_000_u32
        .checked_sub(
            70_000_000_u32
                .checked_sub(*root_size)
                .with_context(|| format!("Root directory size {} too large", root_size))
                .map_err(Error::Value)?,
        )
        .context("There is enough free space already")
        .map_err(Error::Value)?;
    Ok(format!(
        "{}",
        dirs.values()
            .filter(|value| **value >= limit)
            .sorted()
            .next()
            .with_context(|| format!("No directories of at least {} bytes", limit))
            .map_err(Error::Value)?
    ))
}
