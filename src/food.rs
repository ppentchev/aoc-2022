/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Implement the food-related test things.

use std::borrow::ToOwned;
use std::fs;

use anyhow::{anyhow, Context};
use nom::{
    bytes::complete::tag,
    character::complete::u64 as n_u64,
    combinator::all_consuming,
    multi::{many0, separated_list1},
    sequence::terminated,
    IResult, Parser,
};

use crate::defs::{Config, Error};

/// What can a single elf carry?
#[derive(Debug)]
struct Carry {
    /*
    /// The calories of the separate items carried.
    lines: Vec<u64>,
    */
    /// The sum total of the calories carried.
    total: u64,
}

/// Parse a single line containing a number of calories.
fn calories_line(input: &str) -> IResult<&str, u64> {
    terminated(n_u64, tag("\n"))(input)
}

/// Parse the lines representing the burden of a single elf.
fn carry(input: &str) -> IResult<&str, Carry> {
    let (r_input, lines) = many0(calories_line)(input)?;
    let total = lines.iter().sum();
    Ok((r_input, Carry { /*lines,*/ total, }))
}

/// Parse the full input, the burdens of many elves.
fn p_all_carry(input: &str) -> IResult<&str, Vec<Carry>> {
    all_consuming(separated_list1(tag("\n"), carry))(input)
}

/// Parse an input file describing the burdens of many elves.
fn load_burdens<C: Config>(cfg: &C) -> Result<Vec<Carry>, Error> {
    let load_err = |err| Error::Load(format!("{}", cfg.filename().display()), err);
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(load_err)?;
    let (_, all_carry) = p_all_carry
        .parse(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(load_err)?;
    Ok(all_carry)
}

/// Load the burdens of many elves, sort them by the total number of calories.
fn load_top_sorted_by_total<C: Config>(cfg: &C, count: usize) -> Result<Vec<Carry>, Error> {
    let mut all_carry = load_burdens(cfg)?;
    all_carry.sort_unstable_by_key(|carry| carry.total);
    all_carry.reverse();
    all_carry.truncate(count);
    if all_carry.len() != count {
        return Err(Error::Value(anyhow!("Not enough elves")));
    }
    Ok(all_carry)
}

/// Get the sum of the top `count` elf burdens.
///
/// # Errors
/// Propagates errors from [`load_top_sorted_by_total`].
fn sum_up<C: Config>(cfg: &C, count: usize) -> Result<u64, Error> {
    let all_carry = load_top_sorted_by_total(cfg, count)?;
    Ok(all_carry.into_iter().map(|carry| carry.total).sum())
}

/// Figure out how many calories the stupidest elf is burdened with.
///
/// # Errors
/// [`Error::Load`] on filesystem or parse errors.
#[inline]
pub fn test_01_1<C: Config>(cfg: &C) -> Result<String, Error> {
    Ok(format!("{}", sum_up(cfg, 1)?))
}

/// Wait, there is more than one stupid elf?
///
/// # Errors
/// [`Error::Load`] on filesystem or parse errors.
/// [`Error::Value`] if there are fewer than three elves.
#[inline]
pub fn test_01_2<C: Config>(cfg: &C) -> Result<String, Error> {
    Ok(format!("{}", sum_up(cfg, 3)?))
}

#[cfg(test)]
// OVERRIDE: This is a test suite, right?
#[allow(clippy::panic_in_result_fn)]
mod tests {
    use anyhow::{Context, Result};
    use rstest::rstest;

    #[rstest]
    #[case("trivial")]
    #[case("real")]
    fn test_single(#[case] tag: &str) -> Result<()> {
        let (cfg, expected) = crate::tests::read_stuff("01", "1", tag)?;
        assert_eq!(
            super::test_01_1(&cfg).with_context(|| format!("01-1-{} failed", tag))?,
            expected
        );
        Ok(())
    }

    #[rstest]
    #[case("trivial")]
    #[case("real")]
    fn test_three(#[case] tag: &str) -> Result<()> {
        let (cfg, expected) = crate::tests::read_stuff("01", "2", tag)?;
        assert_eq!(
            super::test_01_2(&cfg).with_context(|| format!("01-2-{} failed", tag))?,
            expected
        );
        Ok(())
    }
}
