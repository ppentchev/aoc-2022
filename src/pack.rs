/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Handle packing stuff into the rucksacks.

use std::collections::HashSet;
use std::fs;

use anyhow::{anyhow, Context};
use itertools::Itertools;
use nom::{
    bytes::complete::{is_a, tag},
    combinator::all_consuming,
    error::{Error as NError, ErrorKind as NErrorKind},
    multi::many1,
    sequence::terminated,
    Err as NErr, IResult,
};
use once_cell::sync::Lazy;
use tracing::{error, trace};

use crate::defs::{Config, Error};

/// A single inventory item.
#[derive(Debug, PartialEq, Eq, Hash)]
enum Item {
    /// An item represented by a lowercase letter.
    Lower(u8),

    /// An item represented by an uppercase letter.
    Upper(u8),
}

impl Item {
    /// Get the priority of this inventory item.
    fn prio(&self) -> u32 {
        match *self {
            Self::Lower(value) => value.into(),
            // OVERRIDE: We sincerely hope an u8 converted into an u32 leaves some leeway.
            #[allow(clippy::arithmetic_side_effects)]
            #[allow(clippy::integer_arithmetic)]
            Self::Upper(value) => {
                let base: u32 = value.into();
                base + 26
            }
        }
    }
}

/// The contents of a single rucksack.
#[derive(Debug)]
struct Rucksack {
    /// The contents of each compartment.
    comp: [HashSet<Item>; 2],
}

impl Rucksack {
    /// Get all the items carried by this elf.
    fn all_items(&self) -> HashSet<&Item> {
        self.comp[0].union(&self.comp[1]).collect()
    }
}

/// Bytes that correspond to valid item tags.
static VALID_ITEMS: Lazy<Vec<u8>> = Lazy::new(|| (64..91).chain(96..123).collect());

/// Parse the contents of a single rucksack before splitting it into two compartments.
// OVERRIDE: We can't fix tracing::instrument or IResult
#[allow(clippy::panic_in_result_fn)]
#[tracing::instrument]
// OVERRIDE: We can't fix IResult.
#[allow(clippy::unreachable)]
fn p_rucksack<'data>(input: &'data [u8]) -> IResult<&[u8], Rucksack> {
    let (r_input, codes) = terminated(is_a(&VALID_ITEMS[..]), tag(b"\n"))(input)?;
    let full_len = codes.len();
    trace!(full_len);
    // OVERRIDE: We *know* an usize value can be divided by two, right?
    #[allow(clippy::arithmetic_side_effects)]
    #[allow(clippy::integer_arithmetic)]
    let half_len = codes.len() >> 1_usize;
    trace!(half_len);
    // OVERRIDE: We just divided it by two, we can put it back together again.
    #[allow(clippy::arithmetic_side_effects)]
    #[allow(clippy::integer_arithmetic)]
    let hopefully_full_len = half_len + half_len;
    if full_len != hopefully_full_len {
        error!("full_len {} != {}", full_len, hopefully_full_len);
        return Err(NErr::Failure(NError::new(
            // OVERRIDE: The parser said we can do that.
            #[allow(clippy::indexing_slicing)]
            &input[..full_len],
            NErrorKind::Fail,
        )));
    }

    let byte_to_item = |code: &u8| -> Item {
        if *code > 96 {
            // OVERRIDE: We just checked, did we not?
            #[allow(clippy::arithmetic_side_effects)]
            #[allow(clippy::integer_arithmetic)]
            Item::Lower(*code - 96)
        } else {
            // OVERRIDE: The parser said we can do that.
            #[allow(clippy::arithmetic_side_effects)]
            #[allow(clippy::integer_arithmetic)]
            Item::Upper(*code - 64)
        }
    };
    let bytes_to_items =
        |slc: &'data [u8]| -> HashSet<Item> { slc.iter().map(byte_to_item).collect() };

    // OVERRIDE: The parser said we can do that.
    #[allow(clippy::indexing_slicing)]
    let hash_first: HashSet<Item> = bytes_to_items(&codes[..half_len]);
    trace!(?hash_first);
    // OVERRIDE: The parser said we can do that.
    #[allow(clippy::indexing_slicing)]
    let hash_second: HashSet<Item> = bytes_to_items(&codes[half_len..full_len]);
    trace!(?hash_second);
    Ok((
        r_input,
        Rucksack {
            comp: [hash_first, hash_second],
        },
    ))
}

/// Parse the contents of all the rucksacks.
fn p_rucksacks(input: &[u8]) -> IResult<&[u8], Vec<Rucksack>> {
    all_consuming(many1(p_rucksack))(input)
}

// OVERRIDE: We can't fix tracing::instrument
#[allow(clippy::panic_in_result_fn)]
#[allow(clippy::unreachable)]
#[tracing::instrument]
fn load_packs<C: Config>(cfg: &C) -> Result<Vec<Rucksack>, Error> {
    let load_err = |err| Error::Load(format!("{}", cfg.filename().display()), err);
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(load_err)?;
    let (_, packs) = p_rucksacks(contents.as_bytes())
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(load_err)?;
    Ok(packs)
}

/// Total priority of duplicated items.
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
/// [`Error::Value`] if there is not exactly one item duplicated in each elf's rucksack.
#[inline]
pub fn test_03_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let packs = load_packs(cfg)?;
    let total = packs.into_iter().try_fold(0_u32, |acc, pack| {
        let dups: Vec<&Item> = pack.comp[0]
            .intersection(&pack.comp[1])
            .into_iter()
            .collect();
        match &*dups {
            &[single] => {
                trace!(?single);
                let prio = single.prio();
                Ok(acc
                    .checked_add(prio)
                    .with_context(|| format!("Could not add {} to {}", prio, acc))
                    .map_err(Error::Value)?)
            }
            other => Err(Error::Value(anyhow!(
                "Expected a single duplicate value in {:?}, got {:?}",
                pack,
                other
            ))),
        }
    })?;
    Ok(format!("{}", total))
}

/// Give out badges.
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
/// [`Error::Value`] if there is not exactly one item common among the elves in each group.
#[inline]
pub fn test_03_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let packs = load_packs(cfg)?;
    let mut groups = packs.into_iter().tuples::<(Rucksack, Rucksack, Rucksack)>();
    let total = groups.try_fold(0_u32, |acc, (first, second, third)| {
        let all = {
            let first_all = first.all_items();
            let second_all = second.all_items();
            let third_all = third.all_items();
            let first_second = first_all
                .intersection(&second_all)
                .copied()
                .collect::<HashSet<_>>();
            first_second
                .intersection(&third_all)
                .copied()
                .collect::<Vec<_>>()
        };
        match &*all {
            &[single] => {
                trace!(?single);
                let prio = single.prio();
                Ok(acc
                    .checked_add(prio)
                    .with_context(|| format!("Could not add {} to {}", prio, acc))
                    .map_err(Error::Value)?)
            }
            other => Err(Error::Value(anyhow!(
                "Expected a single common value in {:?} and {:?} and {:?}, got {:?}",
                first,
                second,
                third,
                other
            ))),
        }
    })?;
    Ok(format!("{}", total))
}

#[cfg(test)]
// OVERRIDE: This is a test suite, right?
#[allow(clippy::panic_in_result_fn)]
#[allow(clippy::unwrap_used)]
mod tests {
    use anyhow::{Context, Result};
    use rstest::rstest;
    use tracing_test::traced_test;

    #[rstest]
    #[case("trivial")]
    #[case("real")]
    fn test_dups(#[case] tag: &str) -> Result<()> {
        let (cfg, expected) = crate::tests::read_stuff("03", "1", tag)?;
        assert_eq!(
            super::test_03_1(&cfg).with_context(|| format!("03-1-{} failed", tag))?,
            expected
        );
        Ok(())
    }

    #[rstest]
    #[case("trivial")]
    #[case("real")]
    fn test_badges(#[case] tag: &str) -> Result<()> {
        let (cfg, expected) = crate::tests::read_stuff("03", "2", tag)?;
        assert_eq!(
            super::test_03_2(&cfg).with_context(|| format!("03-2-{} failed", tag))?,
            expected
        );
        Ok(())
    }

    #[rstest]
    #[case("\n")]
    #[case("abc?\n")]
    #[case("Ooof_\n")]
    #[case("Oof_\n")]
    #[case("AaaaAaa")]
    #[traced_test]
    fn test_parse_single_fail(#[case] input: &str) {
        super::p_rucksack(input.as_bytes()).unwrap_err();
    }

    #[rstest]
    #[case("aa\n")]
    #[case("ab\n")]
    #[case("AbcDEf\n")]
    #[case("AaaAaa\n")]
    #[traced_test]
    fn test_parse_single(#[case] input: &str) {
        let (r_input, _) = super::p_rucksack(input.as_bytes()).unwrap();
        assert_eq!(r_input, b"");
    }
}
