#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Implement some Advent of Code 2022 puzzle solvers.

pub mod camp;
pub mod crt;
pub mod defs;
pub mod files;
pub mod food;
pub mod game;
pub mod hanoi;
pub mod jsish;
pub mod monkey;
pub mod pack;
pub mod rope;
pub mod sand;
pub mod signal;
pub mod trees;
pub mod wave;

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::{Path, PathBuf};

    use anyhow::{Context, Result};

    use crate::defs::Config as DefsConfig;

    #[derive(Debug)]
    pub struct Config {
        filename: PathBuf,
    }

    impl DefsConfig for Config {
        fn filename(&self) -> &Path {
            &self.filename
        }
    }

    pub fn read_stuff(day: &str, stage: &str, tag: &str) -> Result<(Config, String)> {
        let infile = format!("data/{}-{}-input.txt", day, tag);
        let outfile = format!("data/{}-{}-{}-output.txt", day, stage, tag);
        let cfg = Config {
            filename: infile
                .try_into()
                .context("Could not parse the input filename")?,
        };
        let expected = fs::read_to_string(&outfile)
            .with_context(|| format!("Could not read the {} output file", outfile))?;
        let first_line = expected
            .split_once('\n')
            .map_or(expected.as_str(), |(first, _)| first)
            .to_owned();
        Ok((cfg, first_line))
    }
}
