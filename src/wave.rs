/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Run a wave through a labyrinth of heights.

use std::collections::{HashMap, HashSet};
use std::fs;
use std::iter;

use anyhow::{anyhow, Context};
use itertools::{FoldWhile, Itertools};
use tracing::trace;

use crate::defs::{Config, Error, MapXY, PosXY};

/// The coordinates of a point on the map.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Pos {
    /// The X coordinate.
    x: usize,

    /// The Y coordinate.
    y: usize,
}

impl PosXY for Pos {
    type Coord = usize;

    fn x(&self) -> Self::Coord {
        self.x
    }

    fn y(&self) -> Self::Coord {
        self.y
    }

    fn from_xy(x: Self::Coord, y: Self::Coord) -> Self {
        Self { x, y }
    }
}

impl Pos {
    /// Figure out what the valid neighbors are.
    fn neighbors(&self, size: Self) -> Vec<Self> {
        iter::once(self.y.checked_sub(1).map(|new_y| Self {
            x: self.x,
            y: new_y,
        }))
        .chain(
            iter::once(self.x.checked_sub(1).map(|new_x| Self {
                x: new_x,
                y: self.y,
            }))
            .chain(
                iter::once(self.x.checked_add(1).and_then(|new_x| {
                    (new_x < size.x).then_some(Self {
                        x: new_x,
                        y: self.y,
                    })
                }))
                .chain(iter::once(self.y.checked_add(1).and_then(|new_y| {
                    (new_y < size.y).then_some(Self {
                        x: self.x,
                        y: new_y,
                    })
                }))),
            ),
        )
        .flatten()
        .collect()
    }
}

/// A representation of the map read from the input file.
#[derive(Debug)]
struct Map {
    /// The map size.
    size: Pos,

    /// The start position.
    start: Pos,

    /// The target position.
    end: Pos,

    /// The heights themselves.
    heights: HashMap<Pos, u8>,
}

impl MapXY<Pos, u8> for Map {
    fn size(&self) -> Pos {
        self.size
    }

    fn start(&self) -> Pos {
        self.start
    }

    fn end(&self) -> Pos {
        self.end
    }

    fn heights(&self) -> &HashMap<Pos, u8> {
        &self.heights
    }
}

/*
/// The character just before 'a' in the ASCII chart.
const CHAR_START: char = '`';

/// The character just after 'z' in the ASCII chart.
const CHAR_END: char = '{';
*/

/// The ASCII code of the letter 'E'.
const BYTE_CAP_E: u8 = 69;

/// The ASCII code of the letter 'S'.
const BYTE_CAP_S: u8 = 83;

/// The value that 'S' should be stored as, one less than 'a'.
const BYTE_START: u8 = 96;

/// The ASCII code of the letter 'a'.
const BYTE_A: u8 = 97;

/// The ASCII code of the letter 'z'.
const BYTE_Z: u8 = 122;

/// The value that 'E' should be stored as, one more than 'z'.
const BYTE_END: u8 = 123;

/// Read a series of lines, parse them into a hashmap.
fn read_map<C: Config>(cfg: &C) -> Result<Map, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let lines = contents.lines().collect::<Vec<_>>();
    let height = lines.len();
    let width = lines
        .first()
        .context("Not even a single line?")
        .map_err(Error::Value)?
        .len();
    if lines.iter().any(|line| line.len() != width) {
        return Err(Error::Value(anyhow!("Not all lines have the same length")));
    }
    let mut heights = lines
        .into_iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.as_bytes().iter().enumerate().map(move |(x, level)| {
                let stored = match *level {
                    BYTE_CAP_S => BYTE_START,
                    BYTE_CAP_E => BYTE_END,
                    good @ BYTE_A..=BYTE_Z => good,
                    other => {
                        return Err(Error::Value(anyhow!(format!(
                            "Invalid byte at ({}, {}): {}",
                            x, y, other
                        ))))
                    }
                };
                Ok((Pos { x, y }, stored))
            })
        })
        .collect::<Result<HashMap<_, _>, _>>()?;
    let start = {
        let found_start = heights
            .iter()
            .filter_map(|(&pos, level)| (*level == BYTE_START).then_some(pos))
            .collect::<Vec<_>>();
        match &*found_start {
            &[ref single] => *single,
            other => {
                return Err(Error::Value(anyhow!(format!(
                    "Expected exactly one start position, found {:?}",
                    other
                ))))
            }
        }
    };
    let end = {
        let found_end = heights
            .iter()
            .filter_map(|(&pos, level)| (*level == BYTE_END).then_some(pos))
            .collect::<Vec<_>>();
        match &*found_end {
            &[ref single] => *single,
            other => {
                return Err(Error::Value(anyhow!(format!(
                    "Expected exactly one end position, found {:?}",
                    other
                ))))
            }
        }
    };
    {
        *(heights
            .get_mut(&start)
            .context("We just validated the start position")
            .map_err(Error::Internal)?) = BYTE_A;
        *(heights
            .get_mut(&end)
            .context("We just validated the end position")
            .map_err(Error::Internal)?) = BYTE_Z;
    }
    Ok(Map {
        size: Pos::from_tuple((width, height)),
        start,
        end,
        heights,
    })
}

/// Run the "wave" algorithm, find a path from the start node to tne end one.
///
/// # Errors
/// TBD
fn run_wave(map: &Map, reversed: bool) -> Result<usize, Error> {
    let (start_map, start_vec) = if reversed {
        (HashSet::from([map.end]), vec![map.end])
    } else {
        (HashSet::from([map.start]), vec![map.start])
    };
    let res = (1..).fold_while(
        (start_map, start_vec, 0),
        |(seen, front, _): (HashSet<Pos>, Vec<Pos>, usize), idx: usize| {
            trace!(idx);
            trace!("{:?}", seen.iter().sorted().collect::<Vec<_>>());
            trace!(?front);
            let new_front: Vec<Pos> = front
                .into_iter()
                .flat_map(|pos| {
                    // OVERRIDE: We have validated all of those, right?
                    #[allow(clippy::arithmetic_side_effects)]
                    #[allow(clippy::integer_arithmetic)]
                    #[allow(clippy::indexing_slicing)]
                    let threshold = if reversed {
                        map.heights[&pos] - 1
                    } else {
                        map.heights[&pos] + 1
                    };
                    let cond = move |value| {
                        if reversed {
                            value >= threshold
                        } else {
                            value <= threshold
                        }
                    };
                    // OVERRIDE: We have tests for that.
                    #[allow(clippy::indexing_slicing)]
                    pos.neighbors(map.size)
                        .into_iter()
                        .filter(move |npos| cond(map.heights[npos]))
                })
                .filter(|pos| !seen.contains(pos))
                .collect::<HashSet<_>>()
                .into_iter()
                .sorted()
                .collect();
            trace!(?new_front);
            let new_seen = new_front.iter().fold(seen, |mut seen_step, pos| {
                seen_step.insert(*pos);
                seen_step
            });
            trace!("{:?}", new_seen.iter().sorted().collect::<Vec<_>>());
            let reached_end = if reversed {
                // OVERRIDE: We just checked that, I believe.
                #[allow(clippy::indexing_slicing)]
                new_front.iter().any(|pos| map.heights[pos] == BYTE_A)
            } else {
                new_seen.contains(&map.end)
            };
            if reached_end || new_front.is_empty() {
                FoldWhile::Done((new_seen, new_front, idx))
            } else {
                FoldWhile::Continue((new_seen, new_front, idx))
            }
        },
    );
    if let FoldWhile::Done((_, _, steps)) = res {
        Ok(steps)
    } else {
        Err(Error::Internal(anyhow!(format!(
            "The wave loop returned {:?}",
            res
        ))))
    }
}

/// Find the length of the shortest path and stuff.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on invalid input data or if a path cannot be found.
#[inline]
pub fn test_12_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let map = read_map(cfg)?;
    Ok(run_wave(&map, false)?.to_string())
}

/// Find the length of the shortest path back to an 'a' square.
///
/// # Errors
/// [`Error::Load`] on filesystem errors.
/// [`Error::Value`] on invalid input data or if a path cannot be found.
#[inline]
pub fn test_12_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let map = read_map(cfg)?;
    Ok(run_wave(&map, true)?.to_string())
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use crate::defs::PosXY;

    use super::Pos;

    #[rstest]
    #[case((0, 0), (5, 5), &[(1, 0), (0, 1)])]
    #[case((1, 0), (5, 5), &[(0, 0), (2, 0), (1, 1)])]
    #[case((2, 3), (5, 5), &[(2, 2), (1, 3), (3, 3), (2, 4)])]
    #[case((2, 4), (5, 5), &[(2, 3), (1, 4), (3, 4)])]
    #[case((4, 3), (5, 5), &[(4, 2), (3, 3), (4, 4)])]
    #[case((4, 4), (5, 5), &[(4, 3), (3, 4)])]
    fn test_neighbors(
        #[case] start: (usize, usize),
        #[case] lab: (usize, usize),
        #[case] expected: &[(usize, usize)],
    ) {
        let neigh = Pos::from_tuple(start).neighbors(Pos::from_tuple(lab));
        assert_eq!(
            neigh,
            expected
                .iter()
                .map(|rpos| Pos::from_tuple(*rpos))
                .collect::<Vec<_>>()
        );
    }
}
