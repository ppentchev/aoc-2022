#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! A possible implementation of the 2022 Advent of Code puzzle solver.

use std::env;
use std::io;
use std::path::{Path, PathBuf};

use anyhow::{bail, Context, Result};
use tracing::Level;

use aoc_2022::camp;
use aoc_2022::crt;
use aoc_2022::defs::{Config as AocConfig, Error as AocError};
use aoc_2022::files;
use aoc_2022::food;
use aoc_2022::game;
use aoc_2022::hanoi;
use aoc_2022::jsish;
use aoc_2022::monkey;
use aoc_2022::pack;
use aoc_2022::rope;
use aoc_2022::sand;
use aoc_2022::signal;
use aoc_2022::trees;
use aoc_2022::wave;

/// Runtime configuration for the Advent of Code solver.
#[derive(Debug)]
struct Config {
    /// The input filename.
    filename: PathBuf,
}

impl AocConfig for Config {
    fn filename(&self) -> &Path {
        &self.filename
    }
}

/// Run the specified test with the specified filename.
///
/// # Errors
/// Dies on string (UTF-8) parsing errors.
fn run<F>(tag: &str, testf: F, filename: &str) -> Result<String>
where
    F: FnOnce(&Config) -> Result<String, AocError>,
{
    {
        let level = if filename.contains("trivial") {
            Level::TRACE
        } else {
            Level::INFO
        };
        let sub = tracing_subscriber::fmt()
            .with_max_level(level)
            .with_writer(io::stderr)
            .finish();
        tracing::subscriber::set_global_default(sub)
            .context("Could not initialize the tracing logger")?;
    }
    let cfg = Config {
        filename: filename
            .try_into()
            .context("Could not parse the input filename")?,
    };
    testf(&cfg).with_context(|| format!("{} failed", tag))
}

// OVERRIDE: This is the main purpose of the test runner.
#[allow(clippy::print_stdout)]
fn main() -> Result<()> {
    let args: Vec<_> = env::args().skip(1).collect();
    println!(
        "{}",
        match args
            .iter()
            .filter_map(|arg| (arg != "--").then_some(arg.as_str()))
            .collect::<Vec<_>>()[..]
        {
            ["implemented"] => [
                "01-1", "01-2", "02-1", "02-2", "03-1", "03-2", "04-1", "04-2", "05-1", "05-2",
                "06-1", "06-2", "07-1", "07-2", "08-1", "08-2", "09-1", "09-2", "10-1", "10-2",
                "11-1", "11-2", "12-1", "12-2", "13-1", "13-2", "14-1", /*"14-2",*/
            ]
            .join("\n"),
            ["01-1", filename] => run("01-1", food::test_01_1::<Config>, filename)?,
            ["01-2", filename] => run("01-2", food::test_01_2::<Config>, filename)?,
            ["02-1", filename] => run("02-1", game::test_02_1::<Config>, filename)?,
            ["02-2", filename] => run("02-2", game::test_02_2::<Config>, filename)?,
            ["03-1", filename] => run("03-1", pack::test_03_1::<Config>, filename)?,
            ["03-2", filename] => run("03-2", pack::test_03_2::<Config>, filename)?,
            ["04-1", filename] => run("04-1", camp::test_04_1::<Config>, filename)?,
            ["04-2", filename] => run("04-2", camp::test_04_2::<Config>, filename)?,
            ["05-1", filename] => run("05-1", hanoi::test_05_1::<Config>, filename)?,
            ["05-2", filename] => run("05-2", hanoi::test_05_2::<Config>, filename)?,
            ["06-1", filename] => run("06-1", signal::test_06_1::<Config>, filename)?,
            ["06-2", filename] => run("06-2", signal::test_06_2::<Config>, filename)?,
            ["07-1", filename] => run("07-1", files::test_07_1::<Config>, filename)?,
            ["07-2", filename] => run("07-2", files::test_07_2::<Config>, filename)?,
            ["08-1", filename] => run("08-1", trees::test_08_1::<Config>, filename)?,
            ["08-2", filename] => run("08-2", trees::test_08_2::<Config>, filename)?,
            ["09-1", filename] => run("09-1", rope::test_09_1::<Config>, filename)?,
            ["09-2", filename] => run("09-2", rope::test_09_2::<Config>, filename)?,
            ["10-1", filename] => run("10-1", crt::test_10_1::<Config>, filename)?,
            ["10-2", filename] => run("10-2", crt::test_10_2::<Config>, filename)?,
            ["11-1", filename] => run("11-1", monkey::test_11_1::<Config>, filename)?,
            ["11-2", filename] => run("11-2", monkey::test_11_2::<Config>, filename)?,
            ["12-1", filename] => run("12-1", wave::test_12_1::<Config>, filename)?,
            ["12-2", filename] => run("12-2", wave::test_12_2::<Config>, filename)?,
            ["13-1", filename] => run("13-1", jsish::test_13_1::<Config>, filename)?,
            ["13-2", filename] => run("13-2", jsish::test_13_2::<Config>, filename)?,
            ["14-1", filename] => run("14-1", sand::test_14_1::<Config>, filename)?,
            ["14-2", filename] => run("14-2", sand::test_14_2::<Config>, filename)?,
            _ => bail!("Usage: aoc-2022 implemented | aoc-2022 test-id input-file"),
        }
    );
    Ok(())
}
