/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! The Rock-Paper-Scissors game the elves play.

use std::fs;

use anyhow::{anyhow, Context};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::space1,
    combinator::all_consuming,
    multi::many1,
    sequence::{pair, terminated},
    IResult,
};

use crate::defs::{Config, Error};

/// A single move, theirs or ours.
#[derive(Debug, Clone, Copy)]
enum MoveKind {
    /// Rock.
    Rock,

    /// Paper.
    Paper,

    /// Scissors.
    Scissors,
}

/// The preset round result.
#[derive(Debug, Clone, Copy)]
enum PresetKind {
    /// We lose.
    Loss,

    /// It is a draw.
    Draw,

    /// We win.
    Win,
}

/// A single strategy round.
#[derive(Debug)]
struct Round {
    /// Their move.
    their_move: MoveKind,

    /// Our move.
    our_move: MoveKind,
}

impl Round {
    /// How many points if we lost?
    const SCORE_LOSS: u32 = 0;

    /// How many points if it was a draw?
    const SCORE_DRAW: u32 = 3;

    /// How many points if we won?
    const SCORE_WIN: u32 = 6;

    /// Calculate the value of our move.
    const fn calc_our_value(&self) -> u32 {
        match self.our_move {
            MoveKind::Rock => 1,
            MoveKind::Paper => 2,
            MoveKind::Scissors => 3,
        }
    }

    /// Did we win?
    const fn calc_win_value(&self) -> u32 {
        // OVERRIDE: nope, it's better this way.
        #[allow(clippy::match_same_arms)]
        match (self.their_move, self.our_move) {
            (MoveKind::Rock, MoveKind::Rock) => Self::SCORE_DRAW,
            (MoveKind::Rock, MoveKind::Paper) => Self::SCORE_WIN,
            (MoveKind::Rock, MoveKind::Scissors) => Self::SCORE_LOSS,
            (MoveKind::Paper, MoveKind::Rock) => Self::SCORE_LOSS,
            (MoveKind::Paper, MoveKind::Paper) => Self::SCORE_DRAW,
            (MoveKind::Paper, MoveKind::Scissors) => Self::SCORE_WIN,
            (MoveKind::Scissors, MoveKind::Rock) => Self::SCORE_WIN,
            (MoveKind::Scissors, MoveKind::Paper) => Self::SCORE_LOSS,
            (MoveKind::Scissors, MoveKind::Scissors) => Self::SCORE_DRAW,
        }
    }
}

/// A single preset round.
#[derive(Debug)]
struct PresetRound {
    /// Their move.
    their_move: MoveKind,

    /// The result we should aim for.
    result: PresetKind,
}

impl PresetRound {
    /// Figure out what our move should be.
    const fn into_round(self) -> Round {
        // OVERRIDE: nope, it's better this way.
        #[allow(clippy::match_same_arms)]
        let our_move = match (self.their_move, self.result) {
            (MoveKind::Rock, PresetKind::Loss) => MoveKind::Scissors,
            (MoveKind::Rock, PresetKind::Draw) => MoveKind::Rock,
            (MoveKind::Rock, PresetKind::Win) => MoveKind::Paper,
            (MoveKind::Paper, PresetKind::Loss) => MoveKind::Rock,
            (MoveKind::Paper, PresetKind::Draw) => MoveKind::Paper,
            (MoveKind::Paper, PresetKind::Win) => MoveKind::Scissors,
            (MoveKind::Scissors, PresetKind::Loss) => MoveKind::Paper,
            (MoveKind::Scissors, PresetKind::Draw) => MoveKind::Scissors,
            (MoveKind::Scissors, PresetKind::Win) => MoveKind::Rock,
        };
        Round {
            their_move: self.their_move,
            our_move,
        }
    }
}

/// The whole strategy.
#[derive(Debug)]
struct Strategy {
    /// The successive rounds.
    rounds: Vec<Round>,
}

impl Strategy {
    /// Get our total result from all rounds.
    fn into_total_value(self) -> Result<u32, Error> {
        let round_values: Vec<u32> = self
            .rounds
            .into_iter()
            .map(|round| -> Result<u32, Error> {
                round
                    .calc_our_value()
                    .checked_add(round.calc_win_value())
                    .ok_or_else(|| {
                        Error::Internal(anyhow!(format!("add overflow for round {:?}", round)))
                    })
            })
            .collect::<Result<_, _>>()?;
        Ok(round_values.into_iter().sum())
    }
}

/// The whole preset strategy.
#[derive(Debug)]
struct PresetStrategy {
    /// The successive rounds.
    rounds: Vec<PresetRound>,
}

impl PresetStrategy {
    /// Figure out what our move should be in each round.
    fn into_strategy(self) -> Strategy {
        Strategy {
            rounds: self
                .rounds
                .into_iter()
                .map(PresetRound::into_round)
                .collect(),
        }
    }
}

/// Parse a 'rock' move.
fn p_their_a(input: &str) -> IResult<&str, MoveKind> {
    let (r_input, _) = tag("A")(input)?;
    Ok((r_input, MoveKind::Rock))
}

/// Parse a 'paper' move.
fn p_their_b(input: &str) -> IResult<&str, MoveKind> {
    let (r_input, _) = tag("B")(input)?;
    Ok((r_input, MoveKind::Paper))
}

/// Parse a 'scissors' move.
fn p_their_c(input: &str) -> IResult<&str, MoveKind> {
    let (r_input, _) = tag("C")(input)?;
    Ok((r_input, MoveKind::Scissors))
}

/// Parse their move ("A", "B", "C").
fn p_their_move(input: &str) -> IResult<&str, MoveKind> {
    alt((p_their_a, p_their_b, p_their_c))(input)
}

/// Parse an "X" move.
fn p_our_x(input: &str) -> IResult<&str, MoveKind> {
    let (r_input, _) = tag("X")(input)?;
    Ok((r_input, MoveKind::Rock))
}

/// Parse a "Y" move.
fn p_our_y(input: &str) -> IResult<&str, MoveKind> {
    let (r_input, _) = tag("Y")(input)?;
    Ok((r_input, MoveKind::Paper))
}

/// Parse a "Z" move.
fn p_our_z(input: &str) -> IResult<&str, MoveKind> {
    let (r_input, _) = tag("Z")(input)?;
    Ok((r_input, MoveKind::Scissors))
}

/// Parse our move ("X", "Y", "Z").
fn p_our_move(input: &str) -> IResult<&str, MoveKind> {
    alt((p_our_x, p_our_y, p_our_z))(input)
}

/// Parse a single round strategy line.
fn p_round(input: &str) -> IResult<&str, Round> {
    let (r_input, (their_move, our_move)) = terminated(
        pair(terminated(p_their_move, space1), p_our_move),
        tag("\n"),
    )(input)?;
    Ok((
        r_input,
        Round {
            their_move,
            our_move,
        },
    ))
}

/// Parse the top-level thing.
fn p_strategy(input: &str) -> IResult<&str, Strategy> {
    let (r_input, rounds) = all_consuming(many1(p_round))(input)?;
    Ok((r_input, Strategy { rounds }))
}

/// Parse a 'loss' preset result.
fn p_preset_x(input: &str) -> IResult<&str, PresetKind> {
    let (r_input, _) = tag("X")(input)?;
    Ok((r_input, PresetKind::Loss))
}

/// Parse a 'draw' preset result.
fn p_preset_y(input: &str) -> IResult<&str, PresetKind> {
    let (r_input, _) = tag("Y")(input)?;
    Ok((r_input, PresetKind::Draw))
}

/// Parse a 'win' preset result.
fn p_preset_z(input: &str) -> IResult<&str, PresetKind> {
    let (r_input, _) = tag("Z")(input)?;
    Ok((r_input, PresetKind::Win))
}

/// Parse a preset result ("X", "Y", "Z").
fn p_preset_result(input: &str) -> IResult<&str, PresetKind> {
    alt((p_preset_x, p_preset_y, p_preset_z))(input)
}

/// Parse a single round strategy line.
fn p_preset_round(input: &str) -> IResult<&str, PresetRound> {
    let (r_input, (their_move, result)) = terminated(
        pair(terminated(p_their_move, space1), p_preset_result),
        tag("\n"),
    )(input)?;
    Ok((r_input, PresetRound { their_move, result }))
}

/// Parse the top-level thing.
fn p_preset_strategy(input: &str) -> IResult<&str, PresetStrategy> {
    let (r_input, rounds) = all_consuming(many1(p_preset_round))(input)?;
    Ok((r_input, PresetStrategy { rounds }))
}

/// Load a strategy file and parse it.
fn load_strategy<C: Config>(cfg: &C) -> Result<Strategy, Error> {
    let load_err = |err| Error::Load(format!("{}", cfg.filename().display()), err);
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(load_err)?;
    let (_, strat) = p_strategy(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(load_err)?;
    Ok(strat)
}

/// Load a preset strategy file and parse it.
fn load_preset_strategy<C: Config>(cfg: &C) -> Result<PresetStrategy, Error> {
    let load_err = |err| Error::Load(format!("{}", cfg.filename().display()), err);
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(load_err)?;
    let (_, strat) = p_preset_strategy(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(load_err)?;
    Ok(strat)
}

/// Calculate the score according to the strategy guide.
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
#[inline]
pub fn test_02_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let strat = load_strategy(cfg)?;
    let total = strat.into_total_value()?;
    Ok(format!("{}", total))
}

/// Calculate the score according to the strategy guide.
///
/// # Errors
/// [`Error::Load`] on filesystem or parsing errors.
#[inline]
pub fn test_02_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let strat = load_preset_strategy(cfg)?.into_strategy();
    let total = strat.into_total_value()?;
    Ok(format!("{}", total))
}

#[cfg(test)]
// OVERRIDE: This is a test suite, right?
#[allow(clippy::panic_in_result_fn)]
mod tests {
    use anyhow::{Context, Result};
    use rstest::rstest;

    #[rstest]
    #[case("trivial")]
    #[case("real")]
    fn test_literal(#[case] tag: &str) -> Result<()> {
        let (cfg, expected) = crate::tests::read_stuff("02", "1", tag)?;
        assert_eq!(
            super::test_02_1(&cfg).with_context(|| format!("02-1-{} failed", tag))?,
            expected
        );
        Ok(())
    }

    #[rstest]
    #[case("trivial")]
    #[case("real")]
    fn test_preset(#[case] tag: &str) -> Result<()> {
        let (cfg, expected) = crate::tests::read_stuff("02", "2", tag)?;
        assert_eq!(
            super::test_02_2(&cfg).with_context(|| format!("02-2-{} failed", tag))?,
            expected
        );
        Ok(())
    }
}
